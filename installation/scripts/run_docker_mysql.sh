#!/bin/bash

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
echo $SCRIPT_DIR

sudo docker run \
    --name mysql \
    -p 3307:3306 \
    -e MYSQL_USER=test \
    -e MYSQL_PASSWORD=test \
    -e MYSQL_DATABASE=test \
    -e MYSQL_ROOT_PASSWORD=1234 \
    -v $SCRIPT_DIR:/etc/mysql/conf.d \
    -d mysql:5.6
