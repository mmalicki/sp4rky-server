from steps.common import get_app_url, log
from steps.rest_calls import do_get, do_post


def create_user_if_doesnt_exist(context, user_name, user_email, user_password):
    """
    :type user_name: str
    :type user_email: str
    :type user_password: str
    :type context: object
    :rtype: dict | None
    """
    users_api_url = get_app_url(relative_url="/users")
    _, all_users = do_get(url=users_api_url)
    if user_email not in all_users:
        status_code, user_response = create_user(user_name, user_email, user_password)
        if status_code == 200 and user_response:
            context.status_code, context.response, context.auth_header = login_user_and_get_token_header(user_name, password=user_password)
        else:
            raise ValueError(
                "Could not create user. Context: user_name={} user_email={} user_password={} status code: {} response: {}".format(
                    user_name, user_email, user_password, status_code, user_response))
    else:
        context.status_code, context.response, context.auth_header = login_user_and_get_token_header(user_name, password=user_password)
    return None


def create_user(user_name, user_email, user_password):
    users_api_url = get_app_url(relative_url="/users")
    request_body = {"name": user_name, "email": user_email, "password": user_password,
                    "repeatedPassword": user_password}
    return do_post(users_api_url, payload=request_body)


def login_user_and_get_token_header(user_name, password):
    """
    :type user_name: str
    :type password: str
    :rtype: tuple[int, dict, dict[str, str]]
    """
    status, token_response = do_post(url=get_app_url("/auth-requests"),
                                     payload={"password": password, "userName": user_name})
    if token_response:
        token_value = token_response.get("token")
        return status, token_response, _build_token_header(user_name, token_value)
    else:
        log("Could not retrieve token for user: {} and password: {}. Response: {}".format(user_name, password,
                                                                                          token_response))
        return status, token_response, {}


def _build_token_header(user_name, token):
    """
    :type user_name: str
    :type token: str
    :rtype: dict[str, str]
    """
    return {"x-auth-name": str(user_name), "x-auth-token": str(token)}
