from behave import when

from steps.common import get_app_url
from steps.rest_calls import do_post_with_auth, do_delete_with_auth, do_put_with_auth, do_get_with_auth


@when('adding post with {message} as message and {image_path} as image path')
def step_impl(context, message, image_path):
    posts_api_url = get_app_url(relative_url="/posts")
    payload = {"message": message, "imagePath": image_path}
    context.status_code, context.response = do_post_with_auth(context, url=posts_api_url, payload=payload)
    context.created_ids.append(context.response.get("id"))


@when('asking for last created post')
def step_impl(context):
    last_created_id = context.created_ids[-1]
    posts_api_url = get_app_url(relative_url="/posts/{}".format(last_created_id))
    context.status_code, context.response = do_get_with_auth(context, url=posts_api_url)


@when('updating last created post with {message} as message and {image_path} as image path')
def step_impl(context, message, image_path):
    last_created_id = context.created_ids[-1]
    posts_api_url = get_app_url(relative_url="/posts/{}".format(last_created_id))
    payload = {"message": message, "imagePath": image_path}
    context.status_code, context.response = do_put_with_auth(context, url=posts_api_url, payload=payload)


@when('deleting post with last created id')
def step_impl(context):
    last_created_id = context.created_ids.pop()
    posts_api_url = get_app_url(relative_url="/posts/{}".format(last_created_id))
    context.status_code, context.response = do_delete_with_auth(context, url=posts_api_url)
