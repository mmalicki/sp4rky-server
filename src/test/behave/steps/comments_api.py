from behave import when

from steps.common import get_app_url
from steps.rest_calls import do_post_with_auth, do_delete_with_auth, do_get_with_auth


@when('adding comment post to last added post with {message} as message and {image_path} as image path')
def step_impl(context, message, image_path):
    last_created_id = context.created_ids[-1]
    posts_api_url = get_app_url(relative_url="/posts/{}/comments".format(last_created_id))
    payload = {"message": message, "imagePath": image_path}
    context.status_code, context.response = do_post_with_auth(context, url=posts_api_url, payload=payload)
    context.created_ids.append(context.response.get("id"))


@when('deleting comment with last added id')
def step_impl(context):
    last_created_id = context.created_ids.pop()
    comments_api_url = get_app_url(relative_url="/comments/{}".format(last_created_id))
    context.status_code, context.response = do_delete_with_auth(context, url=comments_api_url)


@when('asking for comments of post created {calls} calls ago')
def step_impl(context, calls):
    last_created_id = context.created_ids[int(calls) * -1]
    comments_api_url = get_app_url(relative_url="/posts/{}/comments".format(last_created_id))
    context.status_code, context.response = do_get_with_auth(context, url=comments_api_url)
