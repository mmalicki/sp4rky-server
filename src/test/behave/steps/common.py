import json
import os
from logging import getLogger

import requests
from behave import given
from datetime import datetime


@given('application is running')
def step_impl(context):
    context.created_ids = []
    context.auth_header = {}
    context.status_code = None
    context.response = None


def log(message):
    """
    :type message: str
    """
    print("{} | {}".format(datetime.now(), message))


def do_post(user_name, post_content, token):
    posts_api_url = get_app_url(relative_url="/posts")
    request_body = {"userName": user_name, "message": post_content}
    return requests.post(url=posts_api_url, json=request_body, headers=build_token_header(user_name, token))


def store_response_and_id_in_context(context, requests_response):
    logger = getLogger()
    response_content_dict = json.loads(requests_response.content)
    logger.info("response_content_dict: {}".format(response_content_dict))
    response_id = response_content_dict.get("id")
    logger.info("STORING LAST CREATED ID: {}".format(response_id))
    if response_id:
        context.created_ids.append(response_id)
    context.response = requests_response


def do_post_with(endpoint_url, json_dict, context):
    endpoint_full_url = get_app_url(relative_url=endpoint_url)
    return requests.post(url=endpoint_full_url, json=json_dict,
                         headers=build_token_header(context.user_name, context.token))


def get_app_url(relative_url="/"):
    """
    :type relative_url: str
    :rtype: str
    """
    return "http://localhost:{}".format(_get_application_port()) + relative_url


def _get_application_port():
    """
    :rtype: int
    """
    return os.environ.get('APPLICATION_PORT') or 8080
