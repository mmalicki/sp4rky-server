from behave import when, given

from steps.auto_user_login import login_user_and_get_token_header, create_user_if_doesnt_exist, create_user
from steps.common import get_app_url
from steps.rest_calls import do_get, do_delete_with_auth


@when('logged in as {user_name} with password {password} and stored token')
def step_impl(context, user_name, password):
    context.status_code, context.response, context.auth_header = login_user_and_get_token_header(user_name, password)


@when('asking for all users')
def step_impl(context):
    users_api_url = get_app_url(relative_url="/users")
    context.status_code, context.response = do_get(url=users_api_url)


@when('asking for {user_name} user')
def step_impl(context, user_name):
    users_api_url = get_app_url(relative_url="/users/{}".format(user_name))
    stat, resp = do_get(url=users_api_url)
    context.status_code, context.response = stat, resp


@when('creating user named {user_name} with email {user_email} and password {password}')
def step_impl(context, user_name, user_email, password):
    stat, response = create_user(user_name, user_email, password)
    context.status_code, context.response = stat, response


@when('deleting user named {user_name}')
def step_impl(context, user_name):
    users_api_url = get_app_url(relative_url="/users/{}".format(user_name))
    context.status_code, context.response = do_delete_with_auth(context, url=users_api_url)


@given('that user {user_name} with email {user_email} and password {user_password} exists')
def step_impl(context, user_name, user_email, user_password):
    create_user_if_doesnt_exist(user_name, user_email, user_password, context)
