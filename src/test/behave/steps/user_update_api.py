from behave import when

from steps.common import get_app_url
from steps.rest_calls import do_put_with_auth


@when('updating user name {user_name} and email {user_email} and password {password}')
def step_impl(context, user_name, user_email, password):
    update_account_url = get_app_url("/update-account")
    current_user_name = context.auth_header.get("x-auth-name")
    payload = {"email": user_email, "originalName": current_user_name, "name": user_name, "password": password,
               "passwordRepeated": password}
    stat, response = do_put_with_auth(context, update_account_url, payload=payload)
    context.status_code, context.response = stat, response
