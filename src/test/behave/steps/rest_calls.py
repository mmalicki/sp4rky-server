import json
import requests
from behave import when, then
from steps.common import log, get_app_url


@when('sending GET to {endpoint_url} endpoint')
def step_impl(context, endpoint_url):
    endpoint_full_url = get_app_url(relative_url=endpoint_url)
    context.status_code, context.response = do_get(url=endpoint_full_url)


@when('sending POST to {endpoint_url} endpoint with {payload} payload')
def step_impl(context, endpoint_url, payload):
    endpoint_full_url = get_app_url(relative_url=endpoint_url)
    context.status_code, context.response = do_post(url=endpoint_full_url, payload=payload)


@when('sending DELETE to {endpoint_url} with last created id as {param_name} in payload')
def step_impl(context, endpoint_url, param_name):
    last_created_element = context.created_ids.pop()
    payload = {param_name: last_created_element}
    endpoint_full_url = get_app_url(relative_url=endpoint_url)
    context.status_code, context.response = do_delete(url=endpoint_full_url, payload=payload)

@then('response status code should be {status_code}')
def step_impl(context, status_code):
    response_status_code = int(context.status_code)
    assert response_status_code == int(status_code)


@then('context should contain token')
def step_impl(context):
    assert context.auth_header is not {} and context.auth_header is not None


@then('response should contain phrase {expected_phrase}')
def step_impl(context, expected_phrase):
    response_text = str(context.response)
    assert expected_phrase.lower() in response_text.lower()


@then('response should not contain phrase {expected_phrase}')
def step_impl(context, expected_phrase):
    response_text = str(context.response)
    assert expected_phrase.lower() not in response_text.lower()


def do_put(url, payload):
    return _do_generic_call(requests.put, url, None, payload)


def do_post(url, payload):
    return _do_generic_call(requests.post, url, None, payload)


def do_get(url):
    return _do_generic_call(requests.get, url, None, None)


def do_delete(url, payload=None):
    return _do_generic_call(requests.delete, url, None, payload)


def do_put_with_auth(context, url, payload):
    return _do_generic_call(requests.put, url, context.auth_header, payload)


def do_post_with_auth(context, url, payload):
    return _do_generic_call(requests.post, url, context.auth_header, payload)


def do_get_with_auth(context, url):
    return _do_generic_call(requests.get, url, context.auth_header, None)


def do_delete_with_auth(context, url, payload=None):
    return _do_generic_call(requests.delete, url, context.auth_header, payload)


def _do_generic_call(requests_function, url, auth_header=None, payload=None):
    """
    :type auth_header: dict | None
    :type requests_function: callable
    :type url: str
    :type payload: list | dict | float | int | basestring | None
    :rtype: tuple[int, list | dict | float | int | basestring]
    """
    auth_header = auth_header if auth_header else {}
    try:
        auth_header.update({"Content-Type": "application/json"})
        if payload:
            response = requests_function(url=url, data=json.dumps(payload), headers=auth_header)
        else:
            response = requests_function(url=url, headers=auth_header)
        return response.status_code, json.loads(response.content or "{}")
    except Exception as e:
        error_message = "Could not call {} at: {} with params: {} and header: {}. Reason: {}".format(
            requests_function, url, payload, auth_header, e)
        log(error_message)
        raise ValueError(error_message)
