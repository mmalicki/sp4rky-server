Feature: Login REST API

  @login @api
  Scenario: Login API should generate token
    Given application is running
    When creating user named login_api_test with email login_api@test.com and password arthur
    And logged in as login_api_test with password arthur and stored token
    Then response status code should be 200
    And context should contain token
    When deleting user named login_api_test
    Then response status code should be 200


  @login @api
  Scenario: Login API should not generate token with incorrect password
    Given application is running
    When creating user named login_api_test with email login_api@test.com and password arthur
    And logged in as login_api_test with password ARhtUR and stored token
    Then response status code should be 403
    When logged in as login_api_test with password arthur and stored token
    And deleting user named login_api_test
    Then response status code should be 200

  @login @api
  Scenario: Login API should not generate token for non-existing user
    Given application is running
    When logged in as non_existing with password ARhtUR and stored token
    Then response status code should be 404