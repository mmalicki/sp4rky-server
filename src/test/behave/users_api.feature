Feature: Users REST API

  @user @api
  Scenario: Users API should return empty list of users at first
    Given application is running
    When asking for all users
    Then response status code should be 200
    And response should not contain phrase users_api_test

  @user @api
  Scenario: Users API should add user, check that apis return the user and then delete it
    Given application is running
    When creating user named users_api_test with email users_api@test.com and password arthur
    And asking for all users
    Then response status code should be 200
    And response should contain phrase users_api_test
    When asking for users_api_test user
    Then response status code should be 200
    And response should contain phrase users_api_test
    When logged in as users_api_test with password arthur and stored token
    And deleting user named users_api_test
    And asking for all users
    Then response status code should be 200
    Then response should not contain phrase users_api_test

  @user @api
  Scenario: Users API should return 404 on non-existing user when asked for specifics
    Given application is running
    When asking for users_api_test user
    Then response status code should be 404
    And response should not contain phrase users_api_test

  @user @api
  Scenario: Users API should return 401 (unauthorized) on DELETION attempt of non-existing user
    Given application is running
    When creating user named users_api_test with email users_api@test.com and password arthur
    And logged in as users_api_test with password arthur and stored token
    And deleting user named non_existing
    Then response status code should be 401
    When deleting user named users_api_test
    Then response status code should be 200
