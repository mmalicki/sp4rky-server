Feature: User account update API

  @user_update @api
  Scenario: User update account API should update user email
    Given application is running
    When creating user named update_api_test with email update_api@test.com and password arthur
    And logged in as update_api_test with password arthur and stored token
    And updating user name update_api_test and email update_api2@test.com and password arthur
    Then response status code should be 200
    When asking for update_api_test user
    Then response should contain phrase update_api2@test.com
    When deleting user named update_api_test
    Then response status code should be 200

  @user_update @api
  Scenario: User update account API should update user name and require re-login to be authorized
    Given application is running
    When creating user named update_api_test with email update_api@test.com and password arthur
    And logged in as update_api_test with password arthur and stored token
    And updating user name update_api_test2 and email update_api@test.com and password arthur
    Then response status code should be 200
    When asking for update_api_test2 user
    Then response status code should be 200
    When deleting user named update_api_test2
    Then response status code should be 500
    When logged in as update_api_test2 with password arthur and stored token
    Then response status code should be 200
    When deleting user named update_api_test2
    Then response status code should be 200

  @user_update @api
  Scenario: User update account API should update user password and require re-login to be authorized
    Given application is running
    When creating user named update_api_test with email update_api@test.com and password arthur
    And logged in as update_api_test with password arthur and stored token
    And updating user name update_api_test and email update_api@test.com and password arthur2
    Then response status code should be 200
    When asking for update_api_test user
    Then response status code should be 200
    When logged in as update_api_test with password arthur2 and stored token
    Then response status code should be 200
    When deleting user named update_api_test
    Then response status code should be 200