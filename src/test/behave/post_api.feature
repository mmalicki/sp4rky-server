Feature: Posts REST API

  @post @api
  Scenario: Posts API should accept new post
    Given application is running
    When creating user named posts_api_test with email posts_api@test.com and password arthur
    And logged in as posts_api_test with password arthur and stored token
    And adding post with test message as message and /any/path as image path
    Then response status code should be 200
    And response should contain phrase test message
    And response should contain phrase posts_api_test
    When deleting post with last created id
    Then response status code should be 200
    When deleting user named posts_api_test
    Then response status code should be 200

  @post @api
  Scenario: Posts API should permit adding new post from undefined user
    Given application is running
    When adding post with test message as message and /any/path as image path
    Then response status code should be 500

  @post @api
  Scenario: Posts API should not accept new post containing blacklisted word
    Given application is running
    When creating user named posts_api_test with email posts_api@test.com and password arthur
    And logged in as posts_api_test with password arthur and stored token
    And adding post with owies as message and /any/path as image path
    Then response status code should be 406
    When deleting user named posts_api_test
    Then response status code should be 200

  @post @api
  Scenario: Posts API should accept new post and allow to edit it
    Given application is running
    When creating user named posts_api_test with email posts_api@test.com and password arthur
    And logged in as posts_api_test with password arthur and stored token
    And adding post with test message as message and /any/path as image path
    Then response status code should be 200
    When asking for last created post
    Then response status code should be 200
    And response should contain phrase test message
    And response should contain phrase /any/path
    When updating last created post with changed post as message and /new/dir as image path
    Then response status code should be 200
    When asking for last created post
    Then response status code should be 200
    And response should contain phrase changed post
    And response should contain phrase /new/dir
    When deleting post with last created id
    Then response status code should be 200
    When deleting user named posts_api_test
    Then response status code should be 200