Feature: Post comments REST API

  @post @api
  Scenario: Posts API should accept new post
    Given application is running
    When creating user named posts_api_test with email posts_api@test.com and password arthur
    And logged in as posts_api_test with password arthur and stored token
    And adding post with test message as message and /any/path as image path
    And adding comment post to last added post with test comment as message and /comment/path as image path
    Then response status code should be 200
    When deleting comment with last added id
    Then response status code should be 200
    When deleting post with last created id
    Then response status code should be 200
    When deleting user named posts_api_test
    Then response status code should be 200


  @post @api
  Scenario: Posts API should accept new post
    Given application is running
    When creating user named posts_api_test with email posts_api@test.com and password arthur
    And logged in as posts_api_test with password arthur and stored token
    And adding post with test message as message and /any/path as image path
    And adding comment post to last added post with test comment as message and /comment/path as image path
    Then response status code should be 200
    When asking for comments of post created 2 calls ago
    Then response status code should be 200
    And response should contain phrase test comment
    And response should contain phrase /comment/path
    When deleting comment with last added id
    Then response status code should be 200
    When deleting post with last created id
    Then response status code should be 200
    When deleting user named posts_api_test
    Then response status code should be 200