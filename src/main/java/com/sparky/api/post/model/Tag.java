package com.sparky.api.post.model;

import com.google.common.base.Objects;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "tags")
public class Tag {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(nullable = false, unique = true)
    private String name;

    @ManyToMany(mappedBy = "tags")
    private List<Post> posts;

    public Tag() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Post> getPosts() {
        return posts;
    }

    public void setPosts(List<Post> posts) {
        this.posts = posts;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Tag tag = (Tag) o;
        return Objects.equal(id, tag.id) &&
                Objects.equal(name, tag.name);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id, name);
    }

    public static TagBuilder builder() {
        return new TagBuilder();
    }

    public static class TagBuilder {
        private Long id;
        private String name;
        private List<Post> posts;

        private TagBuilder() {
        }

        public TagBuilder withId(Long id) {
            this.id = id;
            return this;
        }

        public TagBuilder withName(String name) {
            this.name = name;
            return this;
        }

        public TagBuilder withPosts(List<Post> posts) {
            this.posts = posts;
            return this;
        }

        public Tag build() {
            Tag tag = new Tag();
            tag.setId(id);
            tag.setName(name);
            tag.setPosts(posts);
            return tag;
        }
    }
}
