package com.sparky.api.post.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.sparky.api.user.model.User;
import org.hibernate.annotations.*;
import org.hibernate.annotations.CascadeType;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name="posts")
public class Post {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    @Cascade({CascadeType.PERSIST})
    private User user;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "post_tags",
            joinColumns = @JoinColumn(name = "post_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "tag_id", referencedColumnName = "id"))
    @Cascade({CascadeType.ALL})
    private Set<Tag> tags;

    @Column(name = "post_date")
    @JsonFormat(shape= JsonFormat.Shape.STRING, pattern="dd-MM-yyyy HH:mm")
    private Date postDate;

    @Column(name = "message", nullable = false)
    private String message;

    @Column(name = "image_path")
    private String imagePath;

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Set<Tag> getTags() {
        return tags;
    }

    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }

    public Date getPostDate() {
        return postDate;
    }

    public void setPostDate(Date postDate) {
        this.postDate = postDate;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static PostBuilder builder() {
        return new PostBuilder();
    }

    public static final class PostBuilder {
        private Long id;
        private User user;
        private Set<Tag> tags;
        private Date postDate;
        private String message;
        private String imagePath;

        private PostBuilder() {
        }

        public static PostBuilder aPost() {
            return new PostBuilder();
        }

        public PostBuilder withId(Long id) {
            this.id = id;
            return this;
        }

        public PostBuilder withUser(User user) {
            this.user = user;
            return this;
        }

        public PostBuilder setTags(Set<Tag> tags) {
            this.tags = tags;
            return this;
        }

        public PostBuilder withPostDate(Date postDate) {
            this.postDate = postDate;
            return this;
        }

        public PostBuilder withMessage(String message) {
            this.message = message;
            return this;
        }

        public PostBuilder withImagePath(String imagePath) {
            this.imagePath = imagePath;
            return this;
        }

        public PostBuilder withTags(Set<Tag> tags) {
            this.tags = tags;
            return this;
        }

        public Post build() {
            Post post = new Post();
            post.setId(id);
            post.setUser(user);
            post.setTags(tags);
            post.setPostDate(postDate);
            post.setMessage(message);
            post.setImagePath(imagePath);
            return post;
        }
    }
}
