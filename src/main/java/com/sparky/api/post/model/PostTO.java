package com.sparky.api.post.model;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.sparky.api.user.model.UserTO;

import java.util.Date;

public class PostTO {
    private Long id;
    private UserTO user;
    @JsonFormat(shape= JsonFormat.Shape.STRING, pattern="dd-MM-yyyy HH:mm")
    private Date postDate;
    private String message;
    private String imagePath;

    public PostTO(Long id, UserTO user, Date postDate, String message, String imagePath) {
        this.user = user;
        this.postDate = postDate;
        this.message = message;
        this.id = id;
        this.imagePath = imagePath;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public UserTO getUser() {
        return user;
    }

    public void setUser(UserTO user) {
        this.user = user;
    }

    public Date getPostDate() {
        return postDate;
    }

    public void setPostDate(Date postDate) {
        this.postDate = postDate;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
