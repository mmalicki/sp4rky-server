package com.sparky.api.post.tags;

import com.google.common.collect.Sets;
import com.sparky.api.post.model.Tag;

import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PostTagExtractor {
    private static final Pattern HASHTAG_PATTERN = Pattern.compile("#([a-zA-Z0-9]+)");
    private static final int HASHTAG_MATCH_GROUP_INDEX = 1;

    public Set<Tag> extractTagsFrom(String postMessage) {
        Matcher hashTagMatcher = HASHTAG_PATTERN.matcher(postMessage);
        Set<Tag> hashTags = Sets.newHashSet();
        while (hashTagMatcher.find()) {
            hashTags.add(Tag.builder()
                    .withName(hashTagMatcher.group(HASHTAG_MATCH_GROUP_INDEX))
                    .build()
            );
        }
        return hashTags;
    }
}
