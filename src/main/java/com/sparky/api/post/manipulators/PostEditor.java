package com.sparky.api.post.manipulators;

import com.sparky.api.post.model.Post;
import com.sparky.api.post.model.PostEditionRequest;

public class PostEditor {
    public Post edit(Post post, PostEditionRequest request) {
        post.setMessage(request.getMessage());
        post.setImagePath(request.getImagePath());
        return post;
    }
}
