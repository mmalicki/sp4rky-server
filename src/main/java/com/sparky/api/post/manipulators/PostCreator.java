package com.sparky.api.post.manipulators;

import com.sparky.api.post.model.Post;
import com.sparky.api.post.model.PostRequest;
import com.sparky.api.post.model.Tag;
import com.sparky.api.post.repo.TagRepository;
import com.sparky.api.post.tags.PostTagExtractor;
import com.sparky.api.user.exceptions.UserNotFoundException;
import com.sparky.api.user.repo.UserRepository;

import java.util.Date;
import java.util.Set;

import static java.util.stream.Collectors.toSet;

public class PostCreator {
    private final UserRepository userRepository;
    private final TagRepository tagRepository;
    private final PostTagExtractor postTagExtractor;

    public PostCreator(UserRepository userRepository, TagRepository tagRepository,
                       PostTagExtractor postTagExtractor) {
        this.userRepository = userRepository;
        this.tagRepository = tagRepository;
        this.postTagExtractor = postTagExtractor;
    }

    public Post createUsing(String username, PostRequest postRequest) {
        Set<Tag> extractedTags = postTagExtractor.extractTagsFrom(postRequest.getMessage());
        return Post.builder()
                .withUser(userRepository.findByName(username).orElseThrow(UserNotFoundException::new))
                .withMessage(postRequest.getMessage())
                .withImagePath(postRequest.getImagePath())
                .withPostDate(new Date())
                .withTags(findTagsByNamePersistingNonexistentOnes(extractedTags))
                .build();
    }


    private Set<Tag> findTagsByNamePersistingNonexistentOnes(Set<Tag> tags) {
        return tags.stream()
                .map(tag -> tagRepository.findByName(tag.getName()).orElseGet(() -> tagRepository.save(tag)))
                .collect(toSet());
    }

}