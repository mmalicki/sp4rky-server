package com.sparky.api.post.manipulators;

import com.sparky.api.post.model.Post;
import com.sparky.api.post.model.PostTO;
import com.sparky.api.user.model.User;
import com.sparky.api.user.model.UserTO;


public class PostToPostTOConverter {
    public PostTO convert(Post post) {
        User user = post.getUser();
        return new PostTO(
                post.getId(),
                new UserTO(user.getName(), user.getEmail(), user.getSignupDate(), user.getRole()),
                post.getPostDate(),
                post.getMessage(),
                post.getImagePath()
        );
    }
}
