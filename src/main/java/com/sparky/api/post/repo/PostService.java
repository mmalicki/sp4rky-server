package com.sparky.api.post.repo;

import com.sparky.api.post.model.PostEditionRequest;
import com.sparky.api.post.model.PostRequest;
import com.sparky.api.post.model.PostTO;

import java.util.List;


public interface PostService {
    List<PostTO> findAll();

    PostTO findById(Long postId);

    List<PostTO> findAllByTag(String tag);

    List<PostTO> findAllByUser(String username);

    PostTO savePost(String username, PostRequest postRequest);

    void deletePost(Long postId);

    PostTO editPost(Long postId, PostEditionRequest postEditionRequest);
}
