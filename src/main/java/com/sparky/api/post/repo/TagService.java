package com.sparky.api.post.repo;

import com.sparky.api.post.model.Tag;

import java.util.List;
import java.util.Optional;

public interface TagService {
    Optional<Tag> findByName(String name);

    List<Tag> findAll();

    Tag saveTag(Tag tag);
}
