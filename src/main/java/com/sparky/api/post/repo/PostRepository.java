package com.sparky.api.post.repo;

import com.sparky.api.post.model.Post;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;


public interface PostRepository extends CrudRepository<Post, Long> {
    Optional<Post> findOneById(Long id);
    List<Post> findAll();

    List<Post> findAllByOrderByPostDateDesc();

    List<Post> findByTags_NameOrderByPostDateDesc(String tagName);

    List<Post> findByUser_NameOrderByPostDateDesc(String username);
}
