package com.sparky.api.post.repo;

import com.google.common.collect.ImmutableList;
import com.sparky.api.post.model.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class TagServiceImpl implements TagService {
    @Autowired
    private TagRepository tagRepository;

    @Override
    public Tag saveTag(Tag tag) {
        return tagRepository
                .findByName(tag.getName())
                .orElseGet(() -> tagRepository.save(tag));
    }

    @Override
    public Optional<Tag> findByName(String name) {
        return tagRepository.findByName(name);
    }

    @Override
    public List<Tag> findAll() {
        return ImmutableList.copyOf(tagRepository.findAll());
    }
}
