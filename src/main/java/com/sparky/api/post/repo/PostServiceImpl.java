package com.sparky.api.post.repo;

import com.sparky.api.post.exceptions.PostNotFoundException;
import com.sparky.api.post.manipulators.PostCreator;
import com.sparky.api.post.manipulators.PostEditor;
import com.sparky.api.post.manipulators.PostToPostTOConverter;
import com.sparky.api.post.model.Post;
import com.sparky.api.post.model.PostEditionRequest;
import com.sparky.api.post.model.PostRequest;
import com.sparky.api.post.model.PostTO;
import com.sparky.api.post.tags.PostTagExtractor;
import com.sparky.api.post.validator.PostValidator;
import com.sparky.api.user.repo.UserRepository;
import com.sparky.common.MessageValidator;
import com.sparky.security.SecurityConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestHeader;

import javax.annotation.PostConstruct;
import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
public class PostServiceImpl implements PostService {
    @Autowired
    private PostRepository postRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private TagRepository tagRepository;

    private PostToPostTOConverter postToPostTOConverter;
    private PostCreator postCreator;
    private MessageValidator postValidator;
    private PostEditor postEditor;
    private PostTagExtractor postTagExtractor;

    @PostConstruct
    public void init() {
        postToPostTOConverter = new PostToPostTOConverter();
        postCreator = new PostCreator(userRepository, tagRepository, new PostTagExtractor());
        postValidator = new PostValidator();
        postEditor = new PostEditor();
    }

    @Override
    public List<PostTO> findAll() {
        return postRepository.findAllByOrderByPostDateDesc().stream().map(post -> postToPostTOConverter.convert(post)).collect(toList());
    }

    @Override
    public List<PostTO> findAllByTag(String tag) {
        return postRepository.findByTags_NameOrderByPostDateDesc(tag).stream().map(post -> postToPostTOConverter.convert(post)).collect(toList());
    }

    @Override
    public List<PostTO> findAllByUser(String username) {
        return postRepository.findByUser_NameOrderByPostDateDesc(username).stream().map(post -> postToPostTOConverter.convert(post)).collect(toList());
    }

    @Override
    public PostTO savePost(@RequestHeader(SecurityConstants.HEADER_NAME) String username,
                           PostRequest postRequest) {
        postValidator.validate(postRequest.getMessage());
        Post post = postCreator.createUsing(username, postRequest);
        return postToPostTOConverter.convert(postRepository.save(post));
    }

    @Override
    public void deletePost(Long postId) {
        postRepository.delete(postId);
    }

    @Override
    public PostTO findById(Long postId) {
        return postToPostTOConverter.convert(postRepository.findOneById(postId).orElseThrow(PostNotFoundException::new));
    }

    @Override
    public PostTO editPost(Long postId, PostEditionRequest postEditionRequest) {
        postValidator.validate(postEditionRequest.getMessage());
        Post postToEdit = postRepository.findOneById(postId).orElseThrow(PostNotFoundException::new);
        Post editedPost = postEditor.edit(postToEdit, postEditionRequest);
        return postToPostTOConverter.convert(postRepository.save(editedPost));
    }
}
