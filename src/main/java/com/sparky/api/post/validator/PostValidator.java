package com.sparky.api.post.validator;

import com.sparky.common.MessageValidator;
import com.sparky.common.ResourceProvider;
import com.sparky.exceptions.MessageNotAppropriate;

import java.util.List;

public class PostValidator implements MessageValidator{

    public PostValidator() {
    }

    public void validate(String comment) {
        List<String> words = new ResourceProvider().loadBlacklistWords();
        validateWords(comment, words);
    }

    private void validateWords(String comment, List<String> words) {
        boolean valid = words.stream().map(String::toLowerCase).noneMatch(s -> s.contains(comment.toLowerCase()));
        if (!valid) {
            throw new MessageNotAppropriate();
        }
    }
}
