package com.sparky.api.post.exceptions;


public final class PostNotFoundException extends RuntimeException {
    private static final String POST_NOT_FOUND = "POST_NOT_FOUND";

    public PostNotFoundException() {
        super(POST_NOT_FOUND);
    }
}
