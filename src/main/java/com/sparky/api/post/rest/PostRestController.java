package com.sparky.api.post.rest;

import com.sparky.api.post.model.PostEditionRequest;
import com.sparky.api.post.model.PostRequest;
import com.sparky.api.post.model.PostTO;
import com.sparky.api.post.repo.PostService;
import com.sparky.api.user.model.User;
import com.sparky.api.user.model.UserTO;
import com.sparky.api.user.repo.UserService;
import com.sparky.common.UnauthorizedException;
import com.sparky.security.SecurityConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.sparky.api.user.model.User.Role.ADMIN;

@RestController
public class PostRestController {
    @Autowired
    private PostService postService;
    @Autowired
    private UserService userService;

    @RequestMapping(value = "/posts", method = RequestMethod.GET)
    public List<PostTO> getAllPosts() {
        return postService.findAll();
    }

    @RequestMapping(value = "/posts", params = "tag", method = RequestMethod.GET)
    public List<PostTO> getAllPostsByTag(@RequestParam("tag") String tag) {
        return postService.findAllByTag(tag);
    }

    @RequestMapping(value = "/posts", params = "user", method = RequestMethod.GET)
    public List<PostTO> getAllPostsByUser(@RequestParam("user") String username) {
        return postService.findAllByUser(username);
    }

    @RequestMapping(value = "/posts", method = RequestMethod.POST)
    public PostTO addPost(@RequestBody PostRequest postRequest,
                          @RequestHeader(SecurityConstants.HEADER_NAME) String username) {
        return postService.savePost(username, postRequest);
    }

    @RequestMapping(value = "/posts/{postId}", method = RequestMethod.DELETE)
    public void deletePost(@PathVariable Long postId,
                           @RequestHeader(SecurityConstants.HEADER_NAME) String usernameFromHeader) {
        if (postService.findById(postId).getUser().getName().equals(usernameFromHeader)
                || userService.findByName(usernameFromHeader).getRole() == ADMIN) {
            postService.deletePost(postId);
        } else {
            throw new UnauthorizedException();
        }
    }

    @RequestMapping(value = "/posts/{postId}", method = RequestMethod.GET)
    public PostTO getPost(@PathVariable Long postId) {
        return postService.findById(postId);
    }

    @RequestMapping(value = "/posts/{postId}", method = RequestMethod.PUT)
    public PostTO editPost(@PathVariable Long postId,
                           @RequestBody PostEditionRequest postEditionRequest,
                           @RequestHeader(SecurityConstants.HEADER_NAME) String usernameFromHeader) {
        if (postService.findById(postId).getUser().getName().equals(usernameFromHeader)
                || userService.findByName(usernameFromHeader).getRole() == ADMIN) {
            return postService.editPost(postId, postEditionRequest);
        } else {
            throw new UnauthorizedException();
        }
    }

}
