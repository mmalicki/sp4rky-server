package com.sparky.api.post.rest;

import com.sparky.api.post.exceptions.PostNotFoundException;
import com.sparky.exceptions.RestExceptionResponse;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import static org.springframework.http.HttpStatus.NOT_FOUND;

@ControllerAdvice
public class PostRestExceptionHandler {
    @ResponseBody
    @ExceptionHandler(PostNotFoundException.class)
    public RestExceptionResponse handlePostNotFoundException(RuntimeException ex) {
        return new RestExceptionResponse(NOT_FOUND, ex.getMessage());
    }
}
