package com.sparky.api.comment.rest;

import com.sparky.api.comment.model.CommentRequest;
import com.sparky.api.comment.model.CommentTO;
import com.sparky.api.comment.repo.CommentService;
import com.sparky.common.UnauthorizedException;
import com.sparky.security.SecurityConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CommentRestController {
    @Autowired
    private CommentService commentService;

    @RequestMapping(value = "/posts/{postId}/comments", method = RequestMethod.POST)
    public CommentTO addComment(@PathVariable("postId") Long postId,
                                @RequestBody CommentRequest commentRequest,
                                @RequestHeader(SecurityConstants.HEADER_NAME) String username) {
        return commentService.saveComment(postId, username, commentRequest);
    }

    @RequestMapping(value = "/comments/{commentId}", method = RequestMethod.DELETE)
    public void deleteComment(@PathVariable("commentId") Long commentId,
                              @RequestHeader(SecurityConstants.HEADER_NAME) String usernameFromHeader) {
        CommentTO comment = commentService.getComment(commentId);
        if (comment.getUser().getName().equals(usernameFromHeader)) {
            commentService.deleteComment(commentId);
        } else {
            throw new UnauthorizedException();
        }
    }

    @RequestMapping(value = "/comments/{commentId}", method = RequestMethod.PUT)
    public CommentTO editComment(@PathVariable("commentId") Long commentId, @RequestBody CommentRequest commentRequest,
                                 @RequestHeader(SecurityConstants.HEADER_NAME) String usernameFromHeader) {
        CommentTO comment = commentService.getComment(commentId);
        if (comment.getUser().getName().equals(usernameFromHeader)) {
            return commentService.editComment(commentId, commentRequest);
        } else {
            throw new UnauthorizedException();
        }
    }

    @RequestMapping(value = "/posts/{postId}/comments", method = RequestMethod.GET)
    public List<CommentTO> getComments(@PathVariable Long postId) {
        return commentService.getCommentsOrderByDateDesc(postId);
    }

    @RequestMapping(value = "/users/{username}/comments", method = RequestMethod.GET)
    public List<CommentTO> getCommentsByUser(@PathVariable String username) {
        return commentService.getCommentsByUserOrderByDateDesc(username);
    }
}
