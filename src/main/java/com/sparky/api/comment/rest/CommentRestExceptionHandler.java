package com.sparky.api.comment.rest;

import com.sparky.api.comment.exceptions.CommentNotFoundException;
import com.sparky.exceptions.MessageNotAppropriate;
import com.sparky.exceptions.ResourceFileNotFoundException;
import com.sparky.exceptions.RestExceptionResponse;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import static org.springframework.http.HttpStatus.NOT_ACCEPTABLE;
import static org.springframework.http.HttpStatus.NOT_FOUND;

@ControllerAdvice
public class CommentRestExceptionHandler {
    @ResponseBody
    @ExceptionHandler(value = CommentNotFoundException.class)
    public RestExceptionResponse handleCommentNotFoundException(RuntimeException ex) {
        return new RestExceptionResponse(NOT_FOUND, ex.getMessage());
    }

    @ResponseBody
    @ResponseStatus()
    @ExceptionHandler(value = MessageNotAppropriate.class)
    public RestExceptionResponse handleMessageNotAppropriateException(RuntimeException ex) {
        return new RestExceptionResponse(NOT_ACCEPTABLE, ex.getMessage());
    }

    @ResponseBody
    @ExceptionHandler(value = ResourceFileNotFoundException.class)
    public RestExceptionResponse handleResourceFileNotFoundException(RuntimeException ex) {
        return new RestExceptionResponse(NOT_FOUND, ex.getMessage());
    }
}
