package com.sparky.api.comment.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.sparky.api.user.model.UserTO;

import java.util.Date;


public class CommentTO {
    private final Long id;
    private final Long postId;
    private final UserTO user;
    private final String message;
    @JsonFormat(shape= JsonFormat.Shape.STRING, pattern="dd-MM-yyyy HH:mm")
    private final Date postedAt;
    private final String imagePath;

    public CommentTO(Long id, Long postId, String message, Date postedAt, UserTO user, String imagePath) {
        this.id = id;
        this.postId = postId;
        this.message = message;
        this.postedAt = postedAt;
        this.user = user;
        this.imagePath = imagePath;
    }

    public Long getId() {
        return id;
    }

    public Long getPostId() {
        return postId;
    }

    public String getImagePath() {
        return imagePath;
    }

    public UserTO getUser() {
        return user;
    }

    public String getMessage() {
        return message;
    }

    public Date getPostedAt() {
        return postedAt;
    }
}
