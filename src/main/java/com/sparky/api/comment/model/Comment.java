package com.sparky.api.comment.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.sparky.api.post.model.Post;
import com.sparky.api.user.model.User;
import org.hibernate.annotations.*;
import org.hibernate.annotations.CascadeType;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;


@Entity
@Table(name = "comments")
public class Comment {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name="message", length = 256)
    private String message;

    @Column(name = "comment_date")
    @JsonFormat(shape= JsonFormat.Shape.STRING, pattern="dd-MM-yyyy HH:mm")
    private Date commentDate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "post_id", referencedColumnName = "id")
    @Cascade({CascadeType.PERSIST})
    private Post post;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    @Cascade({CascadeType.PERSIST})
    private User user;

    @Column(name = "image_path")
    private String imagePath;

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getCommentDate() {
        return commentDate;
    }

    public void setCommentDate(Date commentDate) {
        this.commentDate = commentDate;
    }

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public static CommentBuilder builder() {
        return new CommentBuilder();
    }

    public static final class CommentBuilder {
        private Long id;
        private String message;
        private Date commentDate;
        private Post post;
        private User user;
        private String imagePath;

        private CommentBuilder() {
        }

        public static CommentBuilder aComment() {
            return new CommentBuilder();
        }

        public CommentBuilder withId(Long id) {
            this.id = id;
            return this;
        }

        public CommentBuilder withMessage(String message) {
            this.message = message;
            return this;
        }

        public CommentBuilder withCommentDate(Date commentDate) {
            this.commentDate = commentDate;
            return this;
        }

        public CommentBuilder withPost(Post post) {
            this.post = post;
            return this;
        }

        public CommentBuilder withUser(User user) {
            this.user = user;
            return this;
        }

        public CommentBuilder withImagePath(String imagePath) {
            this.imagePath = imagePath;
            return this;
        }

        public Comment build() {
            Comment comment = new Comment();
            comment.setId(id);
            comment.setMessage(message);
            comment.setCommentDate(commentDate);
            comment.setPost(post);
            comment.setUser(user);
            comment.setImagePath(imagePath);
            return comment;
        }
    }
}
