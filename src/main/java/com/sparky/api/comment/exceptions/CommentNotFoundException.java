package com.sparky.api.comment.exceptions;


public class CommentNotFoundException extends RuntimeException {
    private static final String COMMENT_NOT_FOUND = "COMMENT_NOT_FOUND";

    public CommentNotFoundException() {
        super(COMMENT_NOT_FOUND);
    }
}
