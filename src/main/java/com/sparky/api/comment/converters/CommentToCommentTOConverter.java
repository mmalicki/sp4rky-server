package com.sparky.api.comment.converters;

import com.sparky.api.comment.model.Comment;
import com.sparky.api.comment.model.CommentTO;
import com.sparky.api.post.model.Post;
import com.sparky.api.user.model.User;
import com.sparky.api.user.model.UserTO;


public class CommentToCommentTOConverter {
    public CommentTO convert(Comment comment) {
        User user = comment.getUser();
        Post post = comment.getPost();
        return new CommentTO(
                comment.getId(),
                post.getId(),
                comment.getMessage(),
                comment.getCommentDate(),
                new UserTO(user.getName(), user.getEmail(), user.getSignupDate(), user.getRole()),
                comment.getImagePath()
        );
    }
}
