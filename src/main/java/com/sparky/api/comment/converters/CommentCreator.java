package com.sparky.api.comment.converters;

import com.sparky.api.comment.model.Comment;
import com.sparky.api.comment.model.CommentRequest;
import com.sparky.api.post.exceptions.PostNotFoundException;
import com.sparky.api.post.repo.PostRepository;
import com.sparky.api.user.exceptions.UserNotFoundException;
import com.sparky.api.user.repo.UserRepository;

import java.util.Date;

public class CommentCreator {
    private final PostRepository postRepository;
    private final UserRepository userRepository;

    public CommentCreator(PostRepository postRepository, UserRepository userRepository) {
        this.postRepository = postRepository;
        this.userRepository = userRepository;
    }

    public Comment createFrom(Long postId, String username, CommentRequest commentRequest) {
        return Comment.builder()
                .withImagePath(commentRequest.getImagePath())
                .withMessage(commentRequest.getMessage())
                .withPost(postRepository.findOneById(postId).orElseThrow(PostNotFoundException::new))
                .withUser(userRepository.findByName(username).orElseThrow(UserNotFoundException::new))
                .withCommentDate(new Date())
                .build();
    }
}
