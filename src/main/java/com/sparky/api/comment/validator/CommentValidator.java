package com.sparky.api.comment.validator;

import com.sparky.common.MessageValidator;
import com.sparky.common.ResourceProvider;
import com.sparky.exceptions.MessageNotAppropriate;

import java.util.List;

public class CommentValidator implements MessageValidator{

    public void validate(String comment) {
        List<String> words = new ResourceProvider().loadBlacklistWords();
        validateWords(comment, words);
    }

    private void validateWords(String comment, List<String> words) {
        boolean isValid = words.stream()
                .map(String::toLowerCase)
                .noneMatch(s -> s.contains(comment.toLowerCase()));
        if (!isValid) {
            throw new MessageNotAppropriate();
        }
    }
}
