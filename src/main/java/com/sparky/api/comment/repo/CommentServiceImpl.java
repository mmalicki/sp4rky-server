package com.sparky.api.comment.repo;

import com.sparky.api.comment.converters.CommentCreator;
import com.sparky.api.comment.converters.CommentToCommentTOConverter;
import com.sparky.api.comment.exceptions.CommentNotFoundException;
import com.sparky.api.comment.model.Comment;
import com.sparky.api.comment.model.CommentRequest;
import com.sparky.api.comment.model.CommentTO;
import com.sparky.api.comment.validator.CommentValidator;
import com.sparky.api.post.repo.PostRepository;
import com.sparky.api.user.repo.UserRepository;
import com.sparky.common.MessageValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;

import static java.util.stream.Collectors.toList;


@Service
public class CommentServiceImpl implements CommentService {
    @Autowired
    private PostRepository postRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private CommentRepository commentRepository;

    private CommentCreator commentCreator;
    private CommentToCommentTOConverter converter;
    private MessageValidator commentValidator;

    @PostConstruct
    public void init() {
        converter = new CommentToCommentTOConverter();
        commentCreator = new CommentCreator(postRepository, userRepository);
        commentValidator = new CommentValidator();
    }

    @Override
    public CommentTO saveComment(Long postId, String username, CommentRequest commentRequest) {
        commentValidator.validate(commentRequest.getMessage());
        Comment comment = commentCreator.createFrom(postId, username, commentRequest);
        return converter.convert(commentRepository.save(comment));
    }

    @Override
    public void deleteComment(Long commentId) {
        commentRepository.delete(commentId);
    }

    @Override
    public CommentTO getComment(Long commentId) {
        return converter.convert(commentRepository.findById(commentId).orElseThrow(CommentNotFoundException::new));
    }

    @Override
    public CommentTO editComment(Long commentId, CommentRequest commentEditionRequestion) {
        commentValidator.validate(commentEditionRequestion.getMessage());
        Comment comment = commentRepository.findById(commentId).orElseThrow(CommentNotFoundException::new);
        comment.setMessage(commentEditionRequestion.getMessage());
        //TODO: comment edited at by ...
        return converter.convert(commentRepository.save(comment));
    }


    @Override
    public List<CommentTO> getCommentsOrderByDateDesc(Long postId) {
        return commentRepository.findByPostIdOrderByCommentDateDesc(postId).stream()
                .map(comment -> converter.convert(comment))
                .collect(toList());
    }

    @Override
    public List<CommentTO> getCommentsByUserOrderByDateDesc(String username) {
        return commentRepository.findByUserNameOrderByCommentDateDesc(username).stream()
                .map(comment -> converter.convert(comment))
                .collect(toList());
    }
}
