package com.sparky.api.comment.repo;

import com.sparky.api.comment.model.Comment;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;


public interface CommentRepository extends CrudRepository<Comment, Long> {
    Optional<Comment> findById(Long id);
    List<Comment> findByPostIdOrderByCommentDateDesc(Long postId);
    List<Comment> findByUserNameOrderByCommentDateDesc(String username);
}
