package com.sparky.api.comment.repo;

import com.sparky.api.comment.model.CommentRequest;
import com.sparky.api.comment.model.CommentTO;

import java.util.List;


public interface CommentService {
    CommentTO saveComment(Long postId, String username, CommentRequest commentRequest);
    void deleteComment(Long commentId);
    CommentTO getComment(Long commentId);
    CommentTO editComment(Long commentId, CommentRequest commentRequest);
    List<CommentTO> getCommentsOrderByDateDesc(Long postId);
    List<CommentTO> getCommentsByUserOrderByDateDesc(String username);
}
