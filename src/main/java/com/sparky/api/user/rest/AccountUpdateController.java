package com.sparky.api.user.rest;

import com.sparky.api.user.model.AccountUpdateRequest;
import com.sparky.api.user.repo.UserService;
import com.sparky.api.user.validators.AccountUpdateValidator;
import com.sparky.common.UnauthorizedException;
import com.sparky.security.SecurityConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.sparky.api.user.model.User.Role.ADMIN;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

@RestController
public class AccountUpdateController {
    private final UserService userService;
    private final AccountUpdateValidator accountUpdateValidator;

    @Autowired
    public AccountUpdateController(UserService userService) {
        this.userService = userService;
        this.accountUpdateValidator = new AccountUpdateValidator();
    }

    @RequestMapping(value = "/update-account", method = PUT)
    public void updateAccount(@RequestBody AccountUpdateRequest accountUpdateRequest,
                              @RequestHeader(SecurityConstants.HEADER_NAME) String usernameFromHeader) {
        if (accountUpdateRequest.getOriginalName().equals(usernameFromHeader)
                || userService.findByName(usernameFromHeader).getRole() == ADMIN) {
            accountUpdateValidator.validate(accountUpdateRequest);
            userService.updateUser(accountUpdateRequest);
        }
        else {
            throw new UnauthorizedException();
        }
    }
}

