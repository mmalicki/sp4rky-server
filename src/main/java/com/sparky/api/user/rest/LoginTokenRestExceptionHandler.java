package com.sparky.api.user.rest;

import com.sparky.exceptions.RestExceptionResponse;
import com.sparky.api.user.exceptions.WrongCredentialsException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import static org.springframework.http.HttpStatus.FORBIDDEN;

@ControllerAdvice
public class LoginTokenRestExceptionHandler {
    @ResponseBody
    @ExceptionHandler(WrongCredentialsException.class)
    public RestExceptionResponse handleWrongCredentialsException(RuntimeException ex) {
        return new RestExceptionResponse(FORBIDDEN, ex.getMessage());
    }
}
