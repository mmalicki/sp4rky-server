package com.sparky.api.user.rest;

import com.sparky.api.user.exceptions.IllegalValueException;
import com.sparky.api.user.model.NewLoginTokenRequest;
import com.sparky.api.user.model.NewLoginTokenResponse;
import com.sparky.api.user.repo.LoginTokenService;
import com.sparky.api.user.validators.NewLoginTokenRequestValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;

import static com.google.common.base.Strings.isNullOrEmpty;
import static com.sparky.api.user.exceptions.IllegalValueException.IllegalValueType.NAME_EMPTY;
import static org.springframework.web.bind.annotation.RequestMethod.DELETE;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
public class LoginTokenRestController {
    @Autowired
    private final LoginTokenService loginTokenService;

    private NewLoginTokenRequestValidator newLoginTokenRequestValidator;

    @PostConstruct
    public void init() {
        newLoginTokenRequestValidator = new NewLoginTokenRequestValidator();
    }

    @Autowired
    public LoginTokenRestController(LoginTokenService loginTokenService) {
        this.loginTokenService = loginTokenService;
    }

    @RequestMapping(value = "/auth-requests", method = POST)
    public NewLoginTokenResponse generateNewLoginToken(@RequestBody NewLoginTokenRequest newLoginTokenRequest) {
        newLoginTokenRequestValidator.validate(newLoginTokenRequest);
        return loginTokenService.generateNewLoginToken(newLoginTokenRequest);
    }

    @RequestMapping(value = "/auth-requests", method = DELETE)
    public void eraseLoginToken(@RequestParam(value="user") String userName) {
        if(isNullOrEmpty(userName)) {
            throw new IllegalValueException(NAME_EMPTY);
        }
        loginTokenService.eraseLoginToken(userName);
    }
}
