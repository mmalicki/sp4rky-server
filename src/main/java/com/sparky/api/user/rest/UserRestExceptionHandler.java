package com.sparky.api.user.rest;

import com.sparky.api.user.exceptions.IllegalValueException;
import com.sparky.api.user.exceptions.SuchUserAlreadyExistsException;
import com.sparky.api.user.exceptions.UserNotFoundException;
import com.sparky.api.user.exceptions.UserSignUpRequestIllegalValueException;
import com.sparky.exceptions.RestExceptionResponse;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import static org.springframework.http.HttpStatus.*;

@ControllerAdvice
public class UserRestExceptionHandler {
    @ResponseBody
    @ExceptionHandler({
            UserSignUpRequestIllegalValueException.class,
            IllegalValueException.class
    })
    public RestExceptionResponse handleUserSignUpRequestIllegalValueException(RuntimeException ex) {
        return new RestExceptionResponse(BAD_REQUEST, ex.getMessage());
    }

    @ResponseBody
    @ExceptionHandler(SuchUserAlreadyExistsException.class)
    public RestExceptionResponse handleSuchUserAlreadyExistsException(RuntimeException ex) {
        return new RestExceptionResponse(CONFLICT, ex.getMessage());
    }

    @ResponseBody
    @ExceptionHandler(UserNotFoundException.class)
    public RestExceptionResponse handleUserNotFoundException(RuntimeException ex) {
        return new RestExceptionResponse(NOT_FOUND, ex.getMessage());
    }
}
