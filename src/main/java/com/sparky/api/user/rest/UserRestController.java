package com.sparky.api.user.rest;

import com.sparky.api.user.model.UserSignUpRequest;
import com.sparky.api.user.model.UserTO;
import com.sparky.api.user.repo.UserService;
import com.sparky.common.UnauthorizedException;
import com.sparky.security.SecurityConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.*;

@RestController
@RequestMapping("/users")
public class UserRestController {
    @Autowired
    private final UserService userService;

    @Autowired
    public UserRestController(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(method = POST)
    public UserTO signUp(@RequestBody UserSignUpRequest userSignUpRequest) {
        return userService.signUp(userSignUpRequest);
    }

    @RequestMapping(method = GET)
    public List<UserTO> findAll() {
        return userService.findAll();
    }

    @RequestMapping(value = "/{name}", method = GET)
    public UserTO findByName(@PathVariable("name") String name) {
        return userService.findByName(name);
    }

    @RequestMapping(value = "/{name}", method = DELETE)
    public void removeUser(@PathVariable("name") String name,
                           @RequestHeader(SecurityConstants.HEADER_NAME) String nameFromHeader) {
        if (name.equals(nameFromHeader)) {
            userService.removeUser(name);
        } else {
            throw new UnauthorizedException();
        }
    }
}
