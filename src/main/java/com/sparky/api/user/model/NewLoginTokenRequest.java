package com.sparky.api.user.model;

public class NewLoginTokenRequest {
    private String userName;
    private String password;

    public NewLoginTokenRequest(String userName, String password) {
        this.userName = userName;
        this.password = password;
    }

    private NewLoginTokenRequest() {
    }

    public String getUserName() {
        return userName;
    }

    public String getPassword() {
        return password;
    }
}
