package com.sparky.api.user.model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "users")
public class User {
    private static final Role DEFAULT_ROLE = Role.USER;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(name = "email")
    private String email;
    @Column(name = "password_hash")
    private String passwordHash;
    @Column(name = "name")
    private String name;
    @Column(name = "signup_date")
    private Date signupDate;
    @Column(name = "login_token")
    private String loginToken;
    @Column(name = "role")
    @Enumerated(EnumType.STRING)
    private Role role = DEFAULT_ROLE;

    public User() {
    }

    public Long getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public String getName() {
        return name;
    }

    public Date getSignupDate() {
        return signupDate;
    }

    public String getLoginToken() {
        return loginToken;
    }

    public Role getRole() {
        return role;
    }

    public static UserBuilder builder() {
        return new UserBuilder();
    }

    public UserBuilder mutate() {
        return new UserBuilder().from(this);
    }

    public static class UserBuilder {
        private User entity;

        private UserBuilder() {
            entity = new User();
        }

        public UserBuilder withId(Long id) {
            this.entity.id = id;
            return this;
        }

        public UserBuilder withEmail(String email) {
            this.entity.email = email;
            return this;
        }

        public UserBuilder withPasswordHash(String passwordHash) {
            this.entity.passwordHash = passwordHash;
            return this;
        }

        public UserBuilder withName(String name) {
            this.entity.name = name;
            return this;
        }

        public UserBuilder withSignupDate(Date signupDate) {
            this.entity.signupDate = signupDate;
            return this;
        }

        public UserBuilder withLoginToken(String loginToken) {
            this.entity.loginToken = loginToken;
            return this;
        }

        public UserBuilder withRole(Role role) {
            this.entity.role = role;
            return this;
        }

        public UserBuilder from(User user) {
            entity = user;
            return this;
        }

        public User build() {
            return entity;
        }
    }

    public enum Role {
        ADMIN, USER
    }
}
