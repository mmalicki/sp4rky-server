package com.sparky.api.user.model;

public class NewLoginTokenResponse {
    private final String token;

    public NewLoginTokenResponse(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }
}
