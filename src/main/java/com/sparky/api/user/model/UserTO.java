package com.sparky.api.user.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class UserTO {
    private final String name;
    private final String email;
    @JsonFormat(shape= JsonFormat.Shape.STRING, pattern="dd-MM-yyyy HH:mm")
    private final Date signupDate;
    private final User.Role role;

    public UserTO(String name, String email, Date signupDate, User.Role role) {
        this.name = name;
        this.email = email;
        this.signupDate = signupDate;
        this.role = role;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public Date getSignupDate() {
        return signupDate;
    }

    public User.Role getRole() {
        return role;
    }
}
