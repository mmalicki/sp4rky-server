package com.sparky.api.user.model;

public class UserSignUpRequest {
    private String email;
    private String password;
    private String repeatedPassword;
    private String name;

    public UserSignUpRequest(String email, String password, String repeatedPassword, String name) {
        this.email = email;
        this.password = password;
        this.repeatedPassword = repeatedPassword;
        this.name = name;
    }

    private UserSignUpRequest() {
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getRepeatedPassword() {
        return repeatedPassword;
    }

    public String getName() {
        return name;
    }
}
