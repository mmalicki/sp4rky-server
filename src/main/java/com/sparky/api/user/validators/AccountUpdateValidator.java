package com.sparky.api.user.validators;

import com.sparky.api.user.exceptions.IllegalValueException;
import com.sparky.api.user.model.AccountUpdateRequest;

import static com.sparky.api.user.exceptions.IllegalValueException.IllegalValueType.NAME_EMPTY;
import static com.sparky.api.user.exceptions.IllegalValueException.IllegalValueType.PASSWORDS_NOT_EQUAL;
import static com.sparky.api.user.exceptions.IllegalValueException.IllegalValueType.PASSWORD_EMPTY;

public class AccountUpdateValidator {
    public void validate(AccountUpdateRequest request) {
        validateNameNotEmpty(request);
        validatePasswordNotEmpty(request);
        validatePasswordsEqual(request);
    }

    private void validateNameNotEmpty(AccountUpdateRequest request) {
        if (request.getName().isEmpty()) {
            throw new IllegalValueException(NAME_EMPTY);
        }
    }

    private void validatePasswordNotEmpty(AccountUpdateRequest request) {
        if (request.getPassword().isEmpty()) {
            throw new IllegalValueException(PASSWORD_EMPTY);
        }
    }

    private void validatePasswordsEqual(AccountUpdateRequest request) {
        if (!request.getPassword().equals(request.getPasswordRepeated())) {
            throw new IllegalValueException(PASSWORDS_NOT_EQUAL);
        }
    }
}
