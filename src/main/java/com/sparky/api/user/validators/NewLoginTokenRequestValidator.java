package com.sparky.api.user.validators;

import com.sparky.api.user.exceptions.IllegalValueException;
import com.sparky.api.user.exceptions.IllegalValueException.IllegalValueType;
import com.sparky.api.user.model.NewLoginTokenRequest;

import static com.google.common.base.Strings.isNullOrEmpty;
import static com.sparky.api.user.exceptions.IllegalValueException.IllegalValueType.NAME_EMPTY;
import static com.sparky.api.user.exceptions.IllegalValueException.IllegalValueType.PASSWORD_EMPTY;

public class NewLoginTokenRequestValidator {
    public void validate(NewLoginTokenRequest newLoginTokenRequest) {
        validateLogin(newLoginTokenRequest.getUserName());
        validatePassword(newLoginTokenRequest.getPassword());
    }

    private void validateLogin(String login) {
        validateNotEmpty(login, NAME_EMPTY);
    }

    private void validatePassword(String password) {
        validateNotEmpty(password, PASSWORD_EMPTY);
    }

    private void validateNotEmpty(String text, IllegalValueType possibleIllegalValueType) {
        if (isNullOrEmpty(text)) {
            throw new IllegalValueException(possibleIllegalValueType);
        }
    }
}
