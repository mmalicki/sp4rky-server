package com.sparky.api.user.validators;

import com.sparky.api.user.repo.UserRepository;
import com.sparky.api.user.exceptions.SuchUserAlreadyExistsException;
import com.sparky.api.user.exceptions.UserSignUpRequestIllegalValueException;
import com.sparky.api.user.model.UserSignUpRequest;

import static com.google.common.base.Strings.isNullOrEmpty;
import static com.sparky.api.user.exceptions.SuchUserAlreadyExistsException.ExistenceFactor.EMAIL_TAKEN;
import static com.sparky.api.user.exceptions.SuchUserAlreadyExistsException.ExistenceFactor.NAME_TAKEN;
import static com.sparky.api.user.exceptions.UserSignUpRequestIllegalValueException.IllegalValueType;
import static com.sparky.api.user.exceptions.UserSignUpRequestIllegalValueException.IllegalValueType.*;

public class UserSignUpRequestValidator {
    private final UserRepository userRepository;

    public UserSignUpRequestValidator(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public void validate(UserSignUpRequest userSignUpRequest) {
        validateEmail(userSignUpRequest.getEmail());
        validateName(userSignUpRequest.getName());
        validatePassword(userSignUpRequest.getPassword(), userSignUpRequest.getRepeatedPassword());
    }

    private void validateName(String name) {
        validateNotEmpty(name, NAME_EMPTY);
        validateNameNotTaken(name);
    }

    private void validateNameNotTaken(String email) {
        if (userRepository.findByName(email).isPresent()) {
            throw new SuchUserAlreadyExistsException(NAME_TAKEN);
        }
    }

    private void validateEmail(String email) {
        validateNotEmpty(email, EMAIL_EMPTY);
        validateEmailNotTaken(email);
    }

    private void validateEmailNotTaken(String email) {
        if (userRepository.findByEmail(email).isPresent()) {
            throw new SuchUserAlreadyExistsException(EMAIL_TAKEN);
        }
    }

    private void validatePassword(String password, String repeatedPassword) {
        validateNotEmpty(password, PASSWORD_EMPTY);
        validatePasswordsEqual(password, repeatedPassword);
    }

    private void validateNotEmpty(String text, IllegalValueType illegalValueType) {
        if (isNullOrEmpty(text)) {
            throw new UserSignUpRequestIllegalValueException(illegalValueType);
        }
    }

    private void validatePasswordsEqual(String password, String repeatedPassword) {
        if (!password.equals(repeatedPassword)) {
            throw new UserSignUpRequestIllegalValueException(PASSWORDS_DIFFERENT);
        }
    }
}
