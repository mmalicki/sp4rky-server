package com.sparky.api.user.exceptions;

public class UserNotFoundException extends RuntimeException {
    private static final String USER_NOT_FOUND_MSG = "USER_NOT_FOUND";

    public UserNotFoundException() {
        super(USER_NOT_FOUND_MSG);
    }
}
