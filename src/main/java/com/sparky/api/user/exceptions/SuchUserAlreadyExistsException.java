package com.sparky.api.user.exceptions;

public final class SuchUserAlreadyExistsException extends RuntimeException {
    public SuchUserAlreadyExistsException(ExistenceFactor existenceFactor) {
        super(existenceFactor.name());
    }

    public enum ExistenceFactor {
        EMAIL_TAKEN, NAME_TAKEN
    }
}
