package com.sparky.api.user.exceptions;

public final class UserSignUpRequestIllegalValueException extends RuntimeException {
    public UserSignUpRequestIllegalValueException(IllegalValueType illegalValueType) {
        super(illegalValueType.name());
    }

    public enum IllegalValueType {
        EMAIL_EMPTY, NAME_EMPTY, PASSWORD_EMPTY, PASSWORDS_DIFFERENT
    }
}
