package com.sparky.api.user.exceptions;

public final class IllegalValueException extends RuntimeException {
    public IllegalValueException(IllegalValueType illegalValueType) {
        super(illegalValueType.name());
    }

    public enum IllegalValueType {
        NAME_EMPTY, PASSWORD_EMPTY, PASSWORDS_NOT_EQUAL
    }
}
