package com.sparky.api.user.exceptions;

public class WrongCredentialsException extends RuntimeException {
    private static final String WRONG_CREDENTIALS_MSG = "WRONG_CREDENTIALS";

    public WrongCredentialsException() {
        super(WRONG_CREDENTIALS_MSG);
    }
}
