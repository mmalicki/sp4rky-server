package com.sparky.api.user.repo;

import com.google.common.collect.ImmutableMap;
import com.sparky.api.user.converters.AccountUpdateToModelConverter;
import com.sparky.api.user.converters.UserSignUpRequestToUserConverter;
import com.sparky.api.user.converters.UserToUserToConverter;
import com.sparky.api.user.exceptions.UserNotFoundException;
import com.sparky.api.user.model.AccountUpdateRequest;
import com.sparky.api.user.model.User;
import com.sparky.api.user.model.UserSignUpRequest;
import com.sparky.api.user.model.UserTO;
import com.sparky.api.user.validators.UserSignUpRequestValidator;
import com.sparky.security.SparkyPasswordEncoder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.toList;

@Service
public class UserSpringService implements UserService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private SparkyPasswordEncoder sparkyPasswordEncoder;

    private UserSignUpRequestToUserConverter userSignUpRequestToUserConverter;
    private UserSignUpRequestValidator userSignUpRequestValidator;
    private UserToUserToConverter userToUserToConverter;
    private AccountUpdateToModelConverter accountUpdateToModelConverter;

    @PostConstruct
    public void init() {
        userSignUpRequestToUserConverter = new UserSignUpRequestToUserConverter(sparkyPasswordEncoder);
        userSignUpRequestValidator = new UserSignUpRequestValidator(userRepository);
        userToUserToConverter = new UserToUserToConverter();
        accountUpdateToModelConverter = new AccountUpdateToModelConverter(sparkyPasswordEncoder);
    }

    @Override
    public UserTO signUp(UserSignUpRequest userSignUpRequest) {
        userSignUpRequestValidator.validate(userSignUpRequest);
        User user = userSignUpRequestToUserConverter.convert(userSignUpRequest);
        userRepository.save(user);
        return userToUserToConverter.convert(user);
    }

    @Override
    public UserTO findByName(String name) {
        return userRepository.findByName(name)
                .map(user -> userToUserToConverter.convert(user))
                .orElseThrow(UserNotFoundException::new);
    }

    @Override
    public List<UserTO> findAll() {
        return userRepository.findAll().stream()
                .map(user -> userToUserToConverter.convert(user))
                .collect(toList());
    }

    @Override
    public void removeUser(String name) {
        User user = userRepository.findByName(name).orElseThrow(UserNotFoundException::new);
        userRepository.delete(user);
    }

    @Override
    public Map<String, String> getCredentials(String username) {
        User user = userRepository.findByName(username).orElseThrow(UserNotFoundException::new);
        return ImmutableMap.<String, String>builder()
                .put("login", username)
                .put("token", user.getLoginToken())
                .build();
    }

    @Override
    @Transactional
    public void updateUser(AccountUpdateRequest accountUpdateRequest) {
        userRepository.findByName(accountUpdateRequest.getOriginalName())
            .map(user -> accountUpdateToModelConverter.convert(accountUpdateRequest, user))
            .orElseThrow(UserNotFoundException::new);
    }
}
