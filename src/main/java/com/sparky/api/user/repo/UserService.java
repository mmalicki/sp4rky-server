package com.sparky.api.user.repo;

import com.sparky.api.user.model.AccountUpdateRequest;
import com.sparky.api.user.model.UserSignUpRequest;
import com.sparky.api.user.model.UserTO;

import java.util.List;
import java.util.Map;

public interface UserService {
    UserTO signUp(UserSignUpRequest userSignUpRequest);

    UserTO findByName(String name);

    List<UserTO> findAll();

    void removeUser(String name);

    Map<String, String> getCredentials(String username);

    void updateUser(AccountUpdateRequest accountUpdateRequest);
}
