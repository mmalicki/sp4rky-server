package com.sparky.api.user.repo;

import com.sparky.api.user.model.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends CrudRepository<User, Long> {
    Optional<User> findById(long id);

    Optional<User> findByName(String name);

    Optional<User> findByEmail(String email);

    @Query("select u from User u where u.name = ?1 or u.email = ?1")
    Optional<User> findByLogin(String login);

    List<User> findAll();
}
