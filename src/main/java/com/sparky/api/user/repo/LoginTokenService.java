package com.sparky.api.user.repo;

import com.sparky.api.user.model.NewLoginTokenRequest;
import com.sparky.api.user.model.NewLoginTokenResponse;

public interface LoginTokenService {
    NewLoginTokenResponse generateNewLoginToken(NewLoginTokenRequest newLoginTokenRequest);

    void eraseLoginToken(String userName);
}
