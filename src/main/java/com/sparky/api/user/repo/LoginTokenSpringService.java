package com.sparky.api.user.repo;

import com.sparky.api.user.exceptions.UserNotFoundException;
import com.sparky.api.user.exceptions.WrongCredentialsException;
import com.sparky.api.user.model.NewLoginTokenRequest;
import com.sparky.api.user.model.NewLoginTokenResponse;
import com.sparky.api.user.model.User;
import com.sparky.security.SparkyLoginTokenGenerator;
import com.sparky.security.SparkyPasswordEncoder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class LoginTokenSpringService implements LoginTokenService {
    @Autowired
    private final UserRepository userRepository;
    @Autowired
    private final SparkyPasswordEncoder sparkyPasswordEncoder;
    @Autowired
    private final SparkyLoginTokenGenerator sparkyLoginTokenGenerator;

    @Autowired
    public LoginTokenSpringService(UserRepository userRepository, SparkyPasswordEncoder sparkyPasswordEncoder,
                                   SparkyLoginTokenGenerator sparkyLoginTokenGenerator) {
        this.userRepository = userRepository;
        this.sparkyPasswordEncoder = sparkyPasswordEncoder;
        this.sparkyLoginTokenGenerator = sparkyLoginTokenGenerator;
    }

    @Override
    public NewLoginTokenResponse generateNewLoginToken(NewLoginTokenRequest newLoginTokenRequest) {
        User user = getUserByName(newLoginTokenRequest.getUserName()).orElseThrow(UserNotFoundException::new);
        if (doPasswordsMatch(newLoginTokenRequest.getPassword(), user.getPasswordHash())) {
            String newLoginToken = sparkyLoginTokenGenerator.generateToken();
            updateUserWithNewLoginTokenInRepository(user, newLoginToken);
            return new NewLoginTokenResponse(newLoginToken);
        } else {
            throw new WrongCredentialsException();
        }
    }

    private boolean doPasswordsMatch(String rawPassword, String encodedPassword) {
        return sparkyPasswordEncoder.matches(rawPassword, encodedPassword);
    }

    @Override
    public void eraseLoginToken(String userName) {
        User user = getUserByName(userName).orElseThrow(UserNotFoundException::new);
        String erasedLoginToken = null;
        updateUserWithNewLoginTokenInRepository(user, erasedLoginToken);
    }

    private Optional<User> getUserByName(String name) {
        return userRepository.findByName(name);
    }

    private void updateUserWithNewLoginTokenInRepository(User user, String newLoginToken) {
        User userWithNewLoginToken = User.builder().from(user).withLoginToken(newLoginToken).build();
        userRepository.save(userWithNewLoginToken);
    }
}
