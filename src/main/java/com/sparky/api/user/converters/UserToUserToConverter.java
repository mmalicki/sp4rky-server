package com.sparky.api.user.converters;

import com.sparky.api.user.model.UserTO;
import com.sparky.api.user.model.User;

public class UserToUserToConverter {
    public UserTO convert(User user) {
        return new UserTO(
                user.getName(),
                user.getEmail(),
                user.getSignupDate(),
                user.getRole()
        );
    }
}
