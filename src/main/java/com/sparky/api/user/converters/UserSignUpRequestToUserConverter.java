package com.sparky.api.user.converters;

import com.sparky.security.SparkyPasswordEncoder;
import com.sparky.api.user.model.User;
import com.sparky.api.user.model.UserSignUpRequest;

import java.util.Date;

public class UserSignUpRequestToUserConverter {
    private final SparkyPasswordEncoder passwordEncoder;

    public UserSignUpRequestToUserConverter(SparkyPasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    public User convert(UserSignUpRequest userSignUpRequest) {
        return User.builder()
                .withEmail(userSignUpRequest.getEmail())
                .withPasswordHash(encodePassword(userSignUpRequest.getPassword()))
                .withName(userSignUpRequest.getName())
                .withSignupDate(new Date())
                .build();
    }

    private String encodePassword(String password) {
        return passwordEncoder.encode(password);
    }
}
