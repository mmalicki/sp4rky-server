package com.sparky.api.user.converters;

import com.sparky.api.user.model.AccountUpdateRequest;
import com.sparky.api.user.model.User;
import com.sparky.security.SparkyPasswordEncoder;

public class AccountUpdateToModelConverter {
    private final SparkyPasswordEncoder sparkyPasswordEncoder;

    public AccountUpdateToModelConverter(SparkyPasswordEncoder sparkyPasswordEncoder) {
        this.sparkyPasswordEncoder = sparkyPasswordEncoder;
    }

    public User convert(AccountUpdateRequest accountUpdateRequest, User user) {
        user.mutate()
                .withEmail(accountUpdateRequest.getEmail())
                .withName(accountUpdateRequest.getName())
                .withPasswordHash(sparkyPasswordEncoder.encode(accountUpdateRequest.getPassword()));
        return user;
    }
}
