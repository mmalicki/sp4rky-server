package com.sparky.api.stats;

import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.math.BigInteger;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static java.time.format.DateTimeFormatter.ofPattern;
import static java.util.stream.Collectors.toList;

@Service
public class PostStatsService {
    private static final DateTimeFormatter MONTH_YEAR_PATTERN = ofPattern("MM-yyyy");

    @PersistenceContext
    private EntityManager em;

    public List<JsonFriendlyPostsInMonth> getNumOfPostsByMonth() {
        String query = "select date_format(post_date, '%m-%Y') as MONTH, count(*) " +
                "from posts " +
                "where post_date > date_sub(now(), interval 12 month) " +
                "group by month " +
                "order by post_date";
        return fetchNumOfPostsByMonth(query);
    }

    private List<JsonFriendlyPostsInMonth> fetchNumOfPostsByMonth(String query) {
        return (List<JsonFriendlyPostsInMonth>) em.createNativeQuery(query)
                .<Object>getResultList()
                .stream()
                .map(r -> toPostsInMonth((Object[]) r))
                .sorted()
                .map(r -> JsonFriendlyPostsInMonth.from((PostsInMonth) r))
                .collect(toList());
    }

    private PostsInMonth toPostsInMonth(Object[] r) {
        YearMonth month = YearMonth.parse((String) r[0], MONTH_YEAR_PATTERN);
        int numOfPosts = ((BigInteger) r[1]).intValue();
        return new PostsInMonth(month, numOfPosts);
    }

    static class JsonFriendlyPostsInMonth {
        private final String month;
        private final int numOfPosts;

        JsonFriendlyPostsInMonth(String month, int numOfPosts) {
            this.month = month;
            this.numOfPosts = numOfPosts;
        }

        public String getMonth() {
            return month;
        }

        public int getNumOfPosts() {
            return numOfPosts;
        }

        static JsonFriendlyPostsInMonth from(PostsInMonth postsInMonth) {
            return new JsonFriendlyPostsInMonth(postsInMonth.month.format(MONTH_YEAR_PATTERN), postsInMonth.numOfPosts);
        }
    }

    private static class PostsInMonth implements Comparable {
        private final YearMonth month;
        private final int numOfPosts;

        public PostsInMonth(YearMonth month, int numOfPosts) {
            this.month = month;
            this.numOfPosts = numOfPosts;
        }

        public YearMonth getMonth() {
            return month;
        }

        public int getNumOfPosts() {
            return numOfPosts;
        }


        @Override
        public int compareTo(Object o) {
            PostsInMonth other = (PostsInMonth) o;
            return this.month.compareTo(other.getMonth());
        }
    }
}
