package com.sparky.api.stats;

import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.math.BigInteger;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Maps.newHashMap;
import static java.time.YearMonth.now;
import static java.time.format.DateTimeFormatter.ofPattern;
import static java.time.temporal.ChronoUnit.MONTHS;
import static java.util.stream.Collectors.toList;

@Service
public class TagStatsService {
    private static final DateTimeFormatter MONTH_YEAR_PATTERN = ofPattern("MM-yyyy");

    @PersistenceContext
    private EntityManager em;

    public List<TagPopularity> getTagStatsPerMonth(int numOfTagsToFetchStatsFor) {
        // FIXME IT GRADUATE
        String query =
                "select TAG, date_format(post_date, '%m-%Y') as MONTH, count(*) as NUM_OF_POSTS " +
                        "from " +
                        "  (select post_tags.post_id as post_id, tags.name as TAG " +
                        "   from post_tags join tags on post_tags.tag_id = tags.id ) as TAG_BY_POST_ID " +
                        "    join posts on TAG_BY_POST_ID.post_id = posts.id " +
                        "where" +
                        "  post_date > date_sub(now(), interval 12 month)" +
                        "  and " +
                        "  TAG in " +
                        "    (select TAG from " +
                        "      (" +
                        "        select TAG, count(*) as NUM_OF_POSTS" +
                        "        from (select post_tags.post_id as post_id, tags.name as TAG " +
                        "            from post_tags join tags on post_tags.tag_id = tags.id ) as TAG_BY_POST_ID " +
                        "        join posts on TAG_BY_POST_ID.post_id = posts.id" +
                        "        group by TAG" +
                        "        order by NUM_OF_POSTS" +
                        "        limit " + numOfTagsToFetchStatsFor +
                        "      ) as MOST_POPULAR_TAGS" +
                        "    )" +
                        "group by TAG, MONTH " +
                        "order by TAG, month(post_date), NUM_OF_POSTS";
        return fetchTagPopularityStatsGroupedByTag(query);
    }

    public List<TagPopularity> getLastYearTagStatsPerMonth(String tagName) {
        String query =
                "select TAG, date_format(post_date, '%m-%Y') as MONTH, count(*) as NUM_OF_POSTS " +
                        "from (" +
                        "  select post_tags.post_id as post_id, tags.name as TAG " +
                        "  from post_tags join tags on post_tags.tag_id = tags.id " +
                        "  where tags.name = '" + tagName + "') as TAG_BY_POST_ID " +
                        "join posts on TAG_BY_POST_ID.post_id = posts.id " +
                        "where post_date > date_sub(now(), interval 12 month)" +
                        "group by TAG, MONTH ";
        return fetchTagPopularityStatsGroupedByTag(query);
    }

    private List<TagPopularity> fetchTagPopularityStatsGroupedByTag(String query) {
        return mergeByTagName(fetchTagPopularityStats(query));
    }

    private List<TagPopularityInMonth> fetchTagPopularityStats(String query) {
        return (List<TagPopularityInMonth>) em.createNativeQuery(query)
                .<Object>getResultList()
                .stream()
                .map(r -> toTagPopularityInMonth((Object[]) r))
                .collect(toList());
    }

    private TagPopularityInMonth toTagPopularityInMonth(Object[] r) {
        String tagName = (String) r[0];
        YearMonth monthId = YearMonth.parse((String) r[1], MONTH_YEAR_PATTERN);
        int numOfPosts = ((BigInteger) r[2]).intValue();
        return new TagPopularityInMonth(tagName, new NumOfPostsInMonth(monthId, numOfPosts));
    }

    private List<TagPopularity> mergeByTagName(List<TagPopularityInMonth> tagsPopularityInMonths) {
        Map<String, List<NumOfPostsInMonth>> tagNameToNumberOfPostsPerMonth = newHashMap();
        tagsPopularityInMonths.forEach(t -> tagNameToNumberOfPostsPerMonth.put(t.getTagName(), newArrayList()));
        tagsPopularityInMonths.forEach(t -> {
            tagNameToNumberOfPostsPerMonth
                    .get(t.getTagName())
                    .add(t.getPostsInMonths());
        });
        return sortByMonthNumber(addMissingMonthsWithNoTagActivity(tagNameToNumberOfPostsPerMonth))
                .entrySet()
                .stream()
                .map(e -> new TagPopularity(e.getKey(), e.getValue()))
                .collect(toList());
    }

    private Map<String, List<NumOfPostsInMonth>> sortByMonthNumber(Map<String, List<NumOfPostsInMonth>> mapping) {
        mapping.values().forEach(Collections::sort);
        return mapping;
    }

    private Map<String, List<NumOfPostsInMonth>> addMissingMonthsWithNoTagActivity(Map<String, List<NumOfPostsInMonth>> mapping) {
        mapping.values().forEach(this::addMissingMonths);
        return mapping;
    }

    private void addMissingMonths(List<NumOfPostsInMonth> npm) {
        List<YearMonth> allMonthsFromYearBackTillNow = allMonthsFromYearBackTillNow();
        List<String> existingMonthsAsStr = npm.stream().map(n -> n.getMonthId().format(MONTH_YEAR_PATTERN)).collect(toList());
        allMonthsFromYearBackTillNow.stream()
                .filter(month -> !existingMonthsAsStr.contains(month.format(MONTH_YEAR_PATTERN)))
                .forEach(month -> npm.add(new NumOfPostsInMonth(month, 0)));
    }

    private List<YearMonth> allMonthsFromYearBackTillNow() {
        YearMonth currentDate = now().minus(12, MONTHS);
        List<YearMonth> months = newArrayList();
        do {
            months.add(currentDate);
            currentDate = currentDate.plus(1, MONTHS);
        }
        while (!currentDate.format(MONTH_YEAR_PATTERN).equals(now().format(MONTH_YEAR_PATTERN)));
        return months;
    }

    public class TagPopularity {
        private final String tag;
        private final List<NumOfPostsInMonth> postsInMonths;

        TagPopularity(String tag, List<NumOfPostsInMonth> postsInMonths) {
            this.tag = tag;
            this.postsInMonths = postsInMonths;
        }

        public String getTag() {
            return tag;
        }

        public List<NumOfPostsInMonth> getPostsInMonths() {
            return postsInMonths;
        }
    }

    class TagPopularityInMonth {
        private final String tag;
        private final NumOfPostsInMonth postsInMonths;

        TagPopularityInMonth(String tag, NumOfPostsInMonth postsInMonths) {
            this.tag = tag;
            this.postsInMonths = postsInMonths;
        }

        public String getTagName() {
            return tag;
        }

        public NumOfPostsInMonth getPostsInMonths() {
            return postsInMonths;
        }
    }

    class NumOfPostsInMonth implements Comparable {
        private final YearMonth monthId;
        private final int numOfPosts;

        NumOfPostsInMonth(YearMonth monthId, int numOfPosts) {
            this.monthId = monthId;
            this.numOfPosts = numOfPosts;
        }

        public YearMonth getMonthId() {
            return monthId;
        }

        public int getNumOfPosts() {
            return numOfPosts;
        }

        @Override
        public int compareTo(Object o) {
            NumOfPostsInMonth nppm = (NumOfPostsInMonth) o;
            return this.monthId.compareTo(nppm.monthId);
        }
    }
}
