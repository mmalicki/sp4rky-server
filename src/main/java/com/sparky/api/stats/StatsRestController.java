package com.sparky.api.stats;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

import static java.time.format.DateTimeFormatter.ofPattern;
import static java.util.Collections.singletonMap;
import static java.util.stream.Collectors.toList;

@RestController
public class StatsRestController {
    @Autowired
    private TagStatsService tagStatsService;
    @Autowired
    private PostStatsService postStatsService;

    @RequestMapping(value = "/stats/tags", method = RequestMethod.GET)
    public Map<String, List<TagPopularity>> getTagStats() {
        int numOfTagsToFetchStatsFor = 10;
        return singletonMap("tags", toTagPopularity(tagStatsService.getTagStatsPerMonth(numOfTagsToFetchStatsFor)));
    }

    @RequestMapping(value = "/stats/tags/{tagname}", method = RequestMethod.GET)
    public Map<String, List<TagPopularity>> getTagStats(@PathVariable("tagname") String tagname) {
        return singletonMap("tags", toTagPopularity(tagStatsService.getLastYearTagStatsPerMonth(tagname)));
    }

    @RequestMapping(value = "/stats/posts")
    public Map<String, List<PostStatsService.JsonFriendlyPostsInMonth>> getPostStats() {
        return singletonMap("postStats", postStatsService.getNumOfPostsByMonth());
    }

    private List<TagPopularity> toTagPopularity(List<TagStatsService.TagPopularity> tagPopularity) {
        return tagPopularity.stream()
                .map(t -> TagPopularity.from(t))
                .collect(toList());
    }

    static class TagPopularity {
        private final String tag;
        private final List<NumOfPostsInMonth> numOfPostsInMonth;

        public TagPopularity(String tag, List<NumOfPostsInMonth> numOfPostsInMonth) {
            this.tag = tag;
            this.numOfPostsInMonth = numOfPostsInMonth;
        }

        public String getTag() {
            return tag;
        }

        public List<NumOfPostsInMonth> getNumOfPostsInMonth() {
            return numOfPostsInMonth;
        }

        static TagPopularity from(TagStatsService.TagPopularity tagPopularity) {
            return new TagPopularity(tagPopularity.getTag(), NumOfPostsInMonth.from(tagPopularity.getPostsInMonths()));
        }
    }

    static class NumOfPostsInMonth {
        private final String month;
        private final int numOfPosts;

        private NumOfPostsInMonth(String month, int numOfPosts) {
            this.month = month;
            this.numOfPosts = numOfPosts;
        }

        public String getMonth() {
            return month;
        }

        public int getNumOfPosts() {
            return numOfPosts;
        }

        static List<NumOfPostsInMonth> from(List<TagStatsService.NumOfPostsInMonth> numOfPostsInMonth) {
            return numOfPostsInMonth.stream()
                    .map(n -> new NumOfPostsInMonth(
                                    n.getMonthId().format(ofPattern("MM-yyyy")),
                                    n.getNumOfPosts()
                            )
                    ).collect(toList());
        }
    }
}
