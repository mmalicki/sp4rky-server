package com.sparky.common;

import com.google.common.base.Charsets;
import com.google.common.collect.ImmutableList;
import com.google.common.io.*;
import com.sparky.exceptions.ResourceFileNotFoundException;

import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class ResourceProvider {

    private static final String BLACKLISTED_WORDS_TXT = "/blacklisted-words.txt";

    public List<String> loadBlacklistWords()  {
        try {
            return Resources
                    .asCharSource(getClass().getResource(BLACKLISTED_WORDS_TXT), Charsets.UTF_8)
                    .readLines();
        } catch (IOException ex) {
            throw new ResourceFileNotFoundException();
        }
    }
}
