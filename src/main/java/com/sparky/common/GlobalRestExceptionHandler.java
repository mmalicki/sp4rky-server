package com.sparky.common;

import com.sparky.exceptions.RestExceptionResponse;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import static org.springframework.http.HttpStatus.UNAUTHORIZED;

@ControllerAdvice
public class GlobalRestExceptionHandler {
    @ResponseBody
    @ExceptionHandler(UnauthorizedException.class)
    public RestExceptionResponse handleUnauthorizedException(RuntimeException ex) {
        return new RestExceptionResponse(UNAUTHORIZED, ex.getMessage());
    }
}
