package com.sparky.common;

public class UnauthorizedException extends RuntimeException {
    public UnauthorizedException() {
        super("UNAUTHORIZED");
    }
}
