package com.sparky.common;

import java.util.List;

public interface MessageValidator {
    void validate(String message);

}
