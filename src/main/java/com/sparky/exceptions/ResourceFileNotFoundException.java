package com.sparky.exceptions;

public class ResourceFileNotFoundException extends RuntimeException{
    private static final String RESOURCE_FILE_NOT_FOUND = "RESOURCE_FILE_NOT_FOUND";

    public ResourceFileNotFoundException() {
        super(RESOURCE_FILE_NOT_FOUND);
    }
}
