package com.sparky.exceptions;

public class MessageNotAppropriate extends RuntimeException {
    private static final String MESSAGE_NOT_APPROPRIATE = "MESSAGE_NOT_APPROPRIATE";

    public MessageNotAppropriate() {
        super(MESSAGE_NOT_APPROPRIATE);
    }
}
