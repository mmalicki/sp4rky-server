package com.sparky.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Map;

import static java.util.Collections.singletonMap;

public final class RestExceptionResponse extends ResponseEntity<Map<String, String>> {
    public RestExceptionResponse(HttpStatus httpStatus, String reason) {
        super(
                singletonMap("reason", reason),
                httpStatus
        );
    }
}
