package com.sparky.security;

import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.math.BigInteger;
import java.security.SecureRandom;

@Service
public class SparkyLoginTokenGeneratorImpl implements SparkyLoginTokenGenerator {
    private SecureRandom random;

    @PostConstruct
    public void init() {
        random = new SecureRandom();
    }

    @Override
    public String generateToken() {
        return new BigInteger(130, random).toString(32);
    }
}
