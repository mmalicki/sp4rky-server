package com.sparky.security;

public interface SecurityConstants {
    String HEADER_NAME = "x-auth-name";
    String HEADER_TOKEN = "x-auth-token";
}
