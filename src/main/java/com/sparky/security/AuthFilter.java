package com.sparky.security;

import com.sparky.api.user.repo.UserService;
import com.sparky.security.requests.DoormanProvider;
import com.sparky.security.requests.rules.RuleFactory;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class AuthFilter implements Filter {
    private DoormanProvider doormanProvider;

    public AuthFilter(UserService userService) {
        this.doormanProvider = new DoormanProvider(new RuleFactory(userService));
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        if (isPermissionGranted(request)) {
            filterChain.doFilter(servletRequest, servletResponse);
        } else {
            response.setStatus(HttpServletResponse.SC_FORBIDDEN);
        }
    }

    private boolean isPermissionGranted(HttpServletRequest request) {
        return doormanProvider.getDoormans().stream()
                .allMatch(doorman -> doorman.isPermissionGranted(request));
    }

    @Override
    public void destroy() {
    }
}
