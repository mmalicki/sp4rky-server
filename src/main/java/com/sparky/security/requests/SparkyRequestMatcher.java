package com.sparky.security.requests;

import javax.servlet.http.HttpServletRequest;

public interface SparkyRequestMatcher {
    boolean matches(HttpServletRequest request);
}
