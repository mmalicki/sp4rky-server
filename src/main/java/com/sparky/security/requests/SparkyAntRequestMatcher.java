package com.sparky.security.requests;

import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.web.servlet.HandlerMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

public class SparkyAntRequestMatcher implements SparkyRequestMatcher {
    private final RequestMatcher requestMatcher;

    public SparkyAntRequestMatcher(String address, String httpMethod) {
        this.requestMatcher = new AntPathRequestMatcher(address, httpMethod);
    }

    public boolean matches(HttpServletRequest request) {
        return requestMatcher.matches(request);
    }
}
