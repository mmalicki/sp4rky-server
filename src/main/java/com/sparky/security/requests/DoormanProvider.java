package com.sparky.security.requests;

import com.google.common.collect.ImmutableList;
import com.sparky.security.requests.rules.RuleFactory;

import java.util.List;

import static org.springframework.http.HttpMethod.DELETE;
import static org.springframework.http.HttpMethod.POST;
import static org.springframework.http.HttpMethod.PUT;

public class DoormanProvider {
    private final RuleFactory ruleFactory;

    public DoormanProvider(RuleFactory ruleFactory) {
        this.ruleFactory = ruleFactory;
    }

    public List<Doorman> getDoormans() {
        return ImmutableList.<Doorman>builder()
                .add(Doorman.builder()
                        .forRequests(
                                new SparkyAntRequestMatcher("/users/**", DELETE.name()),
                                new SparkyAntRequestMatcher("/posts/**", DELETE.name()),
                                new SparkyAntRequestMatcher("/posts/**", PUT.name()),
                                new SparkyAntRequestMatcher("/posts/**", POST.name()),
                                new SparkyAntRequestMatcher("/comments/**", DELETE.name()),
                                new SparkyAntRequestMatcher("/comments/**", PUT.name()),
                                new SparkyAntRequestMatcher("/comments/**", POST.name())
                        )
                        .withRules(ruleFactory.hasValidCredentials())
                        .build()
                )
                .build();
    }
}
