package com.sparky.security.requests.rules;

import com.sparky.api.user.exceptions.WrongCredentialsException;
import com.sparky.api.user.repo.UserService;
import com.sparky.security.SecurityConstants;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;
import java.util.Optional;

import static com.google.common.base.Strings.isNullOrEmpty;
import static java.util.Optional.ofNullable;

public class ValidCredentialsRule implements Rule {
    private final UserService userService;

    public ValidCredentialsRule(UserService userService) {
        this.userService = userService;
    }

    @Override
    public boolean isRuleFulfilledBy(HttpServletRequest request) {
        String name = request.getHeader(SecurityConstants.HEADER_NAME);
        String token = request.getHeader(SecurityConstants.HEADER_TOKEN);
        assertLoginAndTokenPresent(name, token);
        assertCredentialsValid(name, token);
        return true;
    }

    private void assertLoginAndTokenPresent(String login, String token) {
        if (isNullOrEmpty(login) || isNullOrEmpty(token)) {
            throw new RuntimeException("REQUIRED_HEADER_NOT_PRESENT");
        }
    }

    private void assertCredentialsValid(String nameFromHeader, String tokenFromHeader) {
        Map<String, String> credentials = userService.getCredentials(nameFromHeader);
        Optional<String> tokenFromDb = ofNullable(credentials.get("token"));
        if (tokenFromDb.isPresent() && !tokenFromDb.get().equals(tokenFromHeader)) {
            throw new WrongCredentialsException();
        }
    }
}
