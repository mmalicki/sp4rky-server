package com.sparky.security.requests.rules;

import org.springframework.web.servlet.HandlerMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

public class PathVariableRule implements Rule {
    private final String pathVariableName;
    private final String headerName;

    public PathVariableRule(String pathVariableName, String headerName) {
        this.pathVariableName = pathVariableName;
        this.headerName = headerName;
    }

    @Override
    public boolean isRuleFulfilledBy(HttpServletRequest request) {
        Map pathVariables = (Map) request.getAttribute(HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE);
        assertPathVariablePresent(pathVariables, pathVariableName);
        assertHeaderAndPathVariableValuesEqual(
                request.getHeader(headerName),
                (String) pathVariables.get(pathVariableName)
        );
        return true;
    }

    private void assertHeaderAndPathVariableValuesEqual(String headerValue, String pathVariableValue) {
        if (!headerValue.equals(pathVariableValue)) {
            throw new RuntimeException("FORBIDDEN");
        }
    }

    private void assertPathVariablePresent(Map pathVariables, String pathVariableName) {
        if (!pathVariables.containsKey(pathVariableName)) {
            throw new RuntimeException("BAD_REQUEST");
        }
    }
}
