package com.sparky.security.requests.rules;

import javax.servlet.http.HttpServletRequest;

public interface Rule {
    boolean isRuleFulfilledBy(HttpServletRequest request);
}
