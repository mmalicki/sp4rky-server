package com.sparky.security.requests.rules;

import com.sparky.api.user.model.User;
import com.sparky.api.user.model.UserTO;
import com.sparky.api.user.repo.UserService;
import com.sparky.security.SecurityConstants;

import javax.servlet.http.HttpServletRequest;

public class IsAdminRule implements Rule {
    private final UserService userService;

    public IsAdminRule(UserService userService) {
        this.userService = userService;
    }

    @Override
    public boolean isRuleFulfilledBy(HttpServletRequest request) {
        String name = request.getHeader(SecurityConstants.HEADER_NAME);
        UserTO user = userService.findByName(name);
        return user.getRole() == User.Role.ADMIN;
    }
}
