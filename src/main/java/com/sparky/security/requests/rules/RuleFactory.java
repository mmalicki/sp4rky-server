package com.sparky.security.requests.rules;

import com.sparky.api.user.repo.UserService;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;

public class RuleFactory {
    private final UserService userService;

    public RuleFactory(UserService userService) {
        this.userService = userService;
    }

    public Rule hasValidCredentials() {
        return new ValidCredentialsRule(userService);
    }

    public Rule isAdmin() {
        return new IsAdminRule(userService);
    }

    public static Rule any(final Rule... rules) {
        return new Rule() {
            @Override
            public boolean isRuleFulfilledBy(HttpServletRequest request) {
                return Arrays.stream(rules).anyMatch(rule -> rule.isRuleFulfilledBy(request));
            }
        };
    }
}
