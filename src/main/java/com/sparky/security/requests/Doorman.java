package com.sparky.security.requests;

import com.sparky.security.requests.rules.Rule;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.List;

import static com.google.common.collect.Lists.asList;
import static com.google.common.collect.Lists.newArrayList;
import static java.util.Arrays.asList;

public class Doorman {
    private final List<SparkyRequestMatcher> requestMatchers;
    private final List<Rule> rules;

    private Doorman(List<SparkyRequestMatcher> requestMatchers, List<Rule> rules) {
        this.requestMatchers = requestMatchers;
        this.rules = rules;
    }

    public boolean isPermissionGranted(HttpServletRequest request) {
        return !shouldBeCheckedByMe(request) || (shouldBeCheckedByMe(request) && canEnter(request));
    }

    private boolean shouldBeCheckedByMe(HttpServletRequest request) {
        return requestMatchers.stream().anyMatch(matcher -> matcher.matches(request));
    }

    private boolean canEnter(HttpServletRequest request) {
        return rules
                .stream()
                .allMatch(rule -> rule.isRuleFulfilledBy(request));
    }

    public static DoormanBuilder builder() {
        return new DoormanBuilder();
    }

    public static class DoormanBuilder {
        private List<SparkyRequestMatcher> requestMatchers;
        private List<Rule> rules = newArrayList();

        private DoormanBuilder() {
        }

        public DoormanBuilder forRequests(SparkyRequestMatcher... requestMatchers) {
            this.requestMatchers = asList(requestMatchers);
            return this;
        }

        public DoormanBuilder withRules(Rule... rules) {
            this.rules = newArrayList(rules);
            return this;
        }

        public Doorman build() {
            return new Doorman(requestMatchers, rules);
        }
    }
}
