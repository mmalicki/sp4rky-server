package com.sparky.security;

public interface SparkyLoginTokenGenerator {
    String generateToken();
}
