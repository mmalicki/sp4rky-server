package com.sparky;

import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.common.collect.Iterables;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.IntStream;

import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Maps.newTreeMap;
import static com.google.common.collect.Sets.newHashSet;
import static java.lang.String.format;
import static java.util.stream.Collectors.toSet;

public class DummyGenerator {
    //public static void main(String[] args) {
    //    int numOfUsers = 15;
    //    int numOfPosts = 150;

    //    String[] tags = {"muzyka", "psy", "koty", "pytanie", "gry", "polityka", "ciekawostki", "humor", "news", "gluten", "gotowanie"};
    //    Map<String, Integer> tagToIds = tagToIds(tags);

    //    generateUsers(numOfUsers);
    //    generateTags(tagToIds);
    //    generatePosts(numOfUsers, numOfPosts, tagToIds);
    //}

    private static void generateTags(Map<String, Integer> tagToIds) {
        tagToIds.entrySet().stream().forEach(
                e -> System.out.println(
                        format("insert into sparky.tags (id, name) values (%d, '%s');", e.getValue(), e.getKey())
                )
        );
    }

    private static Map<String, Integer> tagToIds(String[] tags) {
        int tagId = 1;
        Map<String, Integer> tagToIds = newTreeMap();
        for (String tag : tags) {
            tagToIds.put(tag, tagId++);
        }
        return tagToIds;
    }

    private static void generatePosts(int numOfUsers, int numOfPosts, Map<String, Integer> tagToIds) {
        for (int postId = 1; postId < numOfPosts; postId++) {
            randomPost(tagToIds, postId, new Random().nextInt(numOfUsers - 1) + 1, randomDateAsString(), "");
        }
    }

    private static void randomPost(Map<String, Integer> tagToIds, int postId, int userId, String date, String imagePath) {
        Set<String> tags = tagToIds.keySet();
        Set<String> tagsToInclude = newHashSet();
        for (int i = 0; i < new Random().nextInt(4) + 1; i++) {
            tagsToInclude.add(random(tags));
        }
        String message = randomMessage(tagsToInclude);

        String postQueryPattern = "insert into sparky.posts (id, user_id, post_date, message, image_path) values (%d, %d, '%s', '%s', '%s');";
        String postTagQueryPattern = "insert into sparky.post_tags (post_id, tag_id) values (%d, %d);";

        System.out.println(format(postQueryPattern, postId, userId, date, message,
                new Random().nextDouble() < 0.6
                        ? "assets/images/" + (new Random().nextInt(16) + 1) + ".jpg"
                        : ""
        ));
        tagsToInclude.forEach(tag -> System.out.println(format(postTagQueryPattern, postId, tagToIds.get(tag))));
    }

    private static String randomMessage(Set<String> tagsToInclude) {
        String text = " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque quis ipsum et sapien euismod consequat. Pellentesque semper nunc enim, quis volutpat risus sollicitudin vel. Aliquam in quam non urna iaculis dapibus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Donec ac lorem ante. Sed dolor sem, ornare eget efficitur ut, mollis et arcu. Nam vitae nisl diam. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Ut semper dolor non orci condimentum luctus. Maecenas at ex metus. Aliquam consectetur, lacus in iaculis elementum, nunc est lacinia tellus, sit amet laoreet odio dui a leo. Cras lectus risus, imperdiet mollis aliquam euismod, fringilla ac nibh. Curabitur eleifend sed purus nec suscipit." +
                "Morbi efficitur sollicitudin lorem, at pretium nunc ultricies ut. Sed bibendum sollicitudin quam quis faucibus. Donec venenatis pellentesque orci id imperdiet. Suspendisse potenti. Mauris scelerisque diam ac egestas fermentum. Proin sit amet mattis quam. Sed dictum nulla id viverra tincidunt." +
                "Nulla facilisi. Fusce maximus ipsum sodales tellus commodo vestibulum. Donec felis est, blandit sit amet neque at, convallis lobortis eros. Duis accumsan sagittis suscipit. Proin egestas fermentum elit sit amet accumsan. Etiam porttitor nisl a tincidunt placerat. Nullam nec dapibus magna, eget aliquet nunc. Cras id sem ipsum. Praesent enim enim, fringilla vel neque in, dapibus fringilla eros. Integer laoreet ex sed odio elementum, vel tempus nisl laoreet." +
                "Proin laoreet dolor leo, imperdiet lacinia magna tempor lobortis. Sed sed urna eu mi finibus consectetur et euismod odio. In non feugiat arcu. Etiam placerat eu nisi non pretium. In euismod dolor tincidunt neque dignissim dapibus. Aliquam sed dictum nibh. Cras a faucibus elit, ultrices finibus orci. Nullam fermentum ligula nec orci cursus iaculis. Vivamus odio enim, tempus elementum nisi non, consectetur dapibus nibh. Etiam eget finibus lorem, sit amet faucibus ante. Nulla lobortis ipsum quis nulla pellentesque laoreet. Aenean non sem quis urna tempor tincidunt vel tincidunt tortor. Ut posuere nisi lacus, vitae ultricies erat hendrerit tincidunt. Etiam pharetra tincidunt porta. Phasellus eget felis arcu. ";
        int beginIndex = new Random().nextInt(text.length() - 130);
        List<String> words = newArrayList(Splitter.on(" ").splitToList(text.substring(beginIndex, beginIndex + 128)));
        tagsToInclude.forEach(tag -> words.add(new Random().nextInt(words.size() - 1), "#" + tag));
        return Joiner.on(" ").join(words);
    }

    private static void generateUsers(int numOfUsers) {
        Set<String> allNames = newHashSet("typicalcobra", "unlawfulfalcon", "sizzlinghorse", "outrageousleopard", "magentahinds", "wingedtortoise", "wronghartebeest", "decimalauk", "unwelcomewildebeest", "sparklingspoonbill", "resonantdove", "adaptablelizard", "passivedotterel", "crabbyseahorse", "wackypeacock", "frivolouscur", "remotekapi", "parchedthrushe", "amiableminnow", "recklessptarmigan", "unkemptbobolink", "worstjay", "quarterlycockroach", "polishedmackerel", "tiresomelark");
        for (int id = 1; id < numOfUsers; id++) {
            String name = removeRandom(allNames);
            System.out.println(newUser(id, name));
        }
    }

    private static String newUser(int id, String name) {
        String signupDate = randomDateAsString();
        return format("insert into sparky.users(id, name, email, password_hash, signup_date, login_token, role) values (%d, '%s', '%s', '%s', '%s', '%s', '%s');",
                id,
                name,
                name + "@gmail.com",
                "$2a$10$ogRH8vB/VcFXa8FBFQwfQ.Ok28H3K1q6USOthK0PVRHHIc9t9OnOa",
                signupDate,
                "",
                "USER"
        );
    }

    private static String randomDateAsString() {
        Random random = new Random();
        int minDate = (int) LocalDateTime.of(2016, 2, 1, 0, 0).toEpochSecond(ZoneOffset.UTC);
        int maxDate = (int) LocalDateTime.now().toEpochSecond(ZoneOffset.UTC);
        long randomDay = minDate + random.nextInt(maxDate - minDate);
        LocalDateTime randomDate = LocalDateTime.ofEpochSecond(randomDay, 0, ZoneOffset.UTC);
        return randomDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"));
    }

    private static <T> T random(Set<T> i) {
        int index = new Random().nextInt(i.size());
        return Iterables.get(i, index);
    }

    private static <T> T removeRandom(Set<T> i) {
        int index = new Random().nextInt(i.size());
        T random = Iterables.get(i, index);
        i.remove(random);
        return random;
    }
}
