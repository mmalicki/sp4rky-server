package com.sparky;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import static springfox.documentation.builders.PathSelectors.regex;

@EnableSwagger2
@SpringBootApplication
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    public Docket updateAccountApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("update-account")
                .apiInfo(apiInfo())
                .select()
                .paths(regex("/update-account.*"))
                .build();
    }

    @Bean
    public Docket usersApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("users")
                .apiInfo(apiInfo())
                .select()
                .paths(regex("/users.*"))
                .build();
    }

    @Bean
    public Docket postsApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("posts")
                .apiInfo(apiInfo())
                .select()
                .paths(regex("/posts.*"))
                .build();
    }

    @Bean
    public Docket commentsApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("comments")
                .apiInfo(apiInfo())
                .select()
                .paths(regex("/comments.*"))
                .build();
    }

    @Bean
    public Docket authRequestApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("auth-requests")
                .apiInfo(apiInfo())
                .select()
                .paths(regex("/auth-requests.*"))
                .build();
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("Sparky backend API")
                .description("Spring-boot-based REST backend API for Sparky")
                .license("The MIT License (MIT)")
                .licenseUrl("https://opensource.org/licenses/MIT")
                .version("1.0")
                .build();
    }
}
