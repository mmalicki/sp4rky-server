package com.sparky;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

import static java.util.Collections.singletonMap;

@RestController
public class HelloController {
    @RequestMapping("/")
    public Map<String, String> index() {
        return singletonMap("status", "ok");
    }
}
